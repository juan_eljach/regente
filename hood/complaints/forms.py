from django import forms
from .models import Complaint

class ComplaintCreateForm(forms.ModelForm):
	class Meta:
		model = Complaint
		fields = ['sender_name', 'sender_email', 'message']