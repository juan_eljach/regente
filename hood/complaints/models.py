from django.db import models
from buildings.models import Building

class Complaint(models.Model):
	building = models.ForeignKey(Building)
	sender_name = models.CharField(max_length=100)
	sender_email = models.EmailField(max_length=254)
	message = models.TextField(max_length=2000)

	def __str__(self):
		return self.sender_name