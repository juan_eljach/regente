from django.contrib import admin
from .models import Complaint

class ComplaintAdmin(admin.ModelAdmin):
	list_display = ('building', 'sender_name', 'sender_email',)

admin.site.register(Complaint, ComplaintAdmin)
# Register your models here.
