from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, TemplateView, ListView
from buildings.models import Building
from .models import Complaint
from .forms import ComplaintCreateForm

class ComplaintCreateView(CreateView):
	model = Complaint
	form_class = ComplaintCreateForm
	success_url = reverse_lazy("thanks")
	template_name = "complaints/create_complaint.html"

	def form_valid(self, form):
		building_slug = self.kwargs["building_slug"]
		building_obj = Building.objects.get(slug__iexact=building_slug)
		form.instance.building = building_obj
		return super(ComplaintCreateView, self).form_valid(form)

	def get_context_data(self, *args, **kwargs):
		context = super(ComplaintCreateView, self).get_context_data(**kwargs)
		building_slug = self.kwargs["building_slug"]
		building_obj = Building.objects.get(slug__iexact=building_slug)
		context["building"] = building_obj
		return context


class ThankYouComplaintView(TemplateView):
	template_name = "complaints/thankyou.html"

class ComplaintListView(ListView):
	template_name = "complaints/complaint_list.html"
	model = Complaint
	context_object_name = "complaints"

	def get_context_data(self, *args, **kwargs):
		context = super(ComplaintListView, self).get_context_data(**kwargs)
		building_slug = self.kwargs["building_slug"]
		building_obj = Building.objects.get(slug__iexact=building_slug)
		context["building"] = building_obj
		return context

	def get_queryset(self, **kwargs):
		building_slug = self.kwargs['building_slug']
		queryset = Complaint.objects.filter(building__slug=building_slug)
		return queryset