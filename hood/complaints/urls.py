from django.conf.urls import patterns, url
from .views import ComplaintCreateView, ComplaintListView, ThankYouComplaintView

urlpatterns = patterns('',
    url(r'^buildings/(?P<building_slug>[-\w]+)/complaints/$', ComplaintListView.as_view(), name='complaints'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/complaints/create/$', ComplaintCreateView.as_view(), name='complaints_create'),
    url(r'^gracias/$', ThankYouComplaintView.as_view(), name='thanks'),
)

