import json
import datetime
import logging
from django.conf import settings
from django.shortcuts import render, get_object_or_404, render_to_response
from django.views.generic import View, ListView, CreateView, DetailView, FormView
from django.views.generic.detail import SingleObjectMixin
from django.http import HttpResponse, HttpResponseForbidden
from django.utils import timezone
from django.template import RequestContext
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from properties.models import Apartment
from buildings.models import Building
from meetns.utils import render_to_json_response
from movements.models import Income
from movements.views import add_income
from .models import AdministrationFee, Payment, BuildingWallet
from .forms import CreateWalletForm
from .tasks import format_price

try:
	mixpanel = settings.MIXPANEL
except:
	mixpanel = False

logger = logging.getLogger(__name__)

def format_price(price):
		price = price.replace(",","")
		price = price.replace(".","")
		price = int(price)

		return price

def create_wallet(request, *args, **kwargs):
	building_slug = kwargs["building_slug"]
	building = get_object_or_404(Building, slug__iexact=building_slug)
	if request.method == "POST":
		print (request.POST)
		form = CreateWalletForm(building, request.POST)
		if form.is_valid():
			wallet = form.save(commit=False)
			wallet.building = building
			wallet.save()

			try:
				if mixpanel:
					mixpanel.track(request.user.id, "Registro de cartera completado")
			except Exception as error:
				logger.error("No se pudo trackear el registro de cartera en Mixpanel: {0}".format(error), exc_info=True, extra={
                    'request': request,
                })

			success_url = reverse("properties", kwargs={'building_slug': building_slug})
			return redirect(success_url)
		else:
			print ("TIENE ERRORES MENOR")
			return render_to_response("wallet/create_wallet.html", RequestContext(request, {"form": form, "building": building}))
	else:
		form = CreateWalletForm(building)
		return render_to_response("wallet/create_wallet.html", RequestContext(request, {"form": form, "building": building}))
	
class AdministrationFeeListView(ListView):
	model = AdministrationFee
	apartment_model = Apartment
	template_name = "wallet/administrationfee_list.html"

	def get_context_data(self, *args, **kwargs):
		context = super(AdministrationFeeListView, self).get_context_data(**kwargs)
		building_slug = self.kwargs['building_slug']
		building_object = Building.objects.get(slug__iexact=building_slug)
		apartments_in_building = self.apartment_model.objects.filter(building=building_object)
		context['building'] = building_object
		context['apartments'] = apartments_in_building
		return context


#be careful at this point. The latest payment MUST be the latest payment, U can not register
#a payment of august and then one of april, cuz the latest will be april and everyting
#will go to shit
#class AdministrationFeeListView(ListView):
#	model = AdministrationFee
#	apartment_model = Apartment
#	template_name = "wallet/administrationfee_list.html"

#	def get_context_data(self, *args, **kwargs):
#		context = super(AdministrationFeeListView, self).get_context_data(**kwargs)
#		building_slug = self.kwargs['building_slug']
#		building_object = Building.objects.get(slug__iexact=building_slug)
#		apartments_in_building = self.apartment_model.objects.filter(building=building_object)
#		context['building'] = building_object
#		apartments_with_delay = []
#		apartments_without_delay = []
#		apartments_without_adminfee = []
#		today = datetime.today()
#		today = str(today.month) + '/' + str(today.day) + '/' + str(today.year)
#		today = datetime.strptime(today, '%m/%d/%Y')

#		for apto in apartments_in_building:
#			try:
#				if apto.administrationfee.payment_set.filter(date_of_payment__lt=today, paid=False):
#					payments_not_paid = apto.administrationfee.payment_set.filter(date_of_payment__lt=today, paid=False)
#					for payment in payments_not_paid:
#						payment_date = payment.date_of_payment
#						payment_date = str(payment_date.month) + '/' + str(payment_date.day) + '/' + str(payment_date.year)
#						payment_date = datetime.strptime(payment_date, '%m/%d/%Y')
#						days_between = abs((today - payment_date).days)
#						if days_between > 30:
#							delay_days = days_between - 30
#							apartments_with_delay.append(apto)
#							break
#				else:
#					apartments_without_delay.append(apto)
#			except self.model.DoesNotExist:
#				apartments_without_adminfee.append(apto)

#		context['apartments_with_delay'] = apartments_with_delay
#		context['apartments_without_delay'] = apartments_without_delay
#		context['apartments_without_adminfee'] = apartments_without_adminfee
#		return context

def get_payments_ajax(request, *args, **kwargs):
	if request.is_ajax():
		q = request.GET.get('term', '')
		today = timezone.now()
		payments = Payment.objects.filter(apartment__name__icontains=q, date_of_payment__month=today.month)
		results =[]
		for payment in payments:
			payment_json = {}
			payment_json["id"] = payment.id
			payment_json["label"] = "Apartamento: {0}".format(payment.apartment.name)
			payment_json["value"] = "Apartamento: {0}".format(payment.apartment.name)
			results.append(payment_json)
		data = json.dumps(results)
	else:
		return HttpResponseForbidden()
	mimetype = 'application/json'
	return HttpResponse(data, mimetype)

class AdministrationFeePaymentsByMonth(DetailView):
	model = Payment
	template_name = "wallet/payments_by_month.html"
	context_object_name = "payment"

	def get_object(self, **kwargs):
		building_slug = self.kwargs['building_slug']
		today = timezone.now()
		try:
			obj = Payment.objects.filter(
				apartment__building__slug=building_slug, 
				date_of_payment__month=today.month, 
				date_of_payment__year=today.year, 
				paid=False).order_by("id").first()
			print ("GOT obj")
		except Exception as lol:
			print (lol)
			obj = None
		return obj

	def get_context_data(self, *args, **kwargs):
		context = super(AdministrationFeePaymentsByMonth, self).get_context_data(*args, **kwargs)
		building_slug = self.kwargs["building_slug"]
		building_object = Building.objects.get(slug__iexact=building_slug)
		context["building"] = building_object
		wallet = BuildingWallet.objects.get(building=building_object)
		context["discount_early_payment"] = format_price(wallet.discount_early_payment_amount)
		today = timezone.now()
		context["current_time"] = today
		return context

class AdministrationFeePaymentListView(ListView):
	model = Payment
	apartment_model = Apartment
	template_name = "wallet/payments_list.html"

	def get_object(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		apartment_pk = self.kwargs['apartment_pk']
		building_object = Building.objects.get(slug__iexact=building_slug)
		obj = self.apartment_model.objects.get(pk=apartment_pk, building=building_object)
		return obj

	def get_context_data(self, *args, **kwargs):
		context = super(AdministrationFeePaymentListView, self).get_context_data(*args, **kwargs)
		apto_obj = self.get_object()
		payments = apto_obj.payment_set.all().order_by("date_of_payment")
		building_slug = self.kwargs['building_slug']
		building_object = Building.objects.get(slug__iexact=building_slug)
		context['building'] = building_object
		context['apartment'] = apto_obj
		context['payments'] = payments
		return context

def mark_as_paid(request, payment_id):
#	if request.method == "POST":
#		post_payment_record_date = request.POST.get("payment_record_date")
#		post_fee_value = request.POST.get("fee_value")
	payment = get_object_or_404(Payment, id=payment_id)
	payment.paid = True
	payment.save()
	payment_record_date = payment.payment_record_date.strftime("%b. %d, %Y")
	concept_of_income = "Cuota de Admon. Apto: {0}".format(payment.apartment.name)
	context = {"paid": payment.paid, "payment_record_date":payment_record_date}
	add_income_result = add_income(
		building=payment.apartment.building, 
		price=payment.fee_value, 
		concept=concept_of_income, 
		date=payment.payment_record_date)
	if add_income_result == True:
		pass
	else:
		context["error"] = True
	return render_to_json_response(context)

def mark_as_paid_inline(request, payment_id):
	if request.method == "POST":
		post_payment_record_date = request.POST.get("payment_record_date")
		post_fee_value = request.POST.get("fee_value")
		date_format = datetime.datetime.strptime(post_payment_record_date, "%m/%d/%Y")
		valid_payment_record_date = datetime.date(year=date_format.year, month=date_format.month, day=date_format.day)
		valid_fee_value = format_price(post_fee_value)

		payment = get_object_or_404(Payment, id=payment_id)
		payment.payment_record_date = valid_payment_record_date
		payment.fee_value = valid_fee_value
		payment.paid = True
		payment.save()
	elif request.method == "GET":
		return HttpResponseForbidden()
	apartment_object = payment.apartment
	context = {}
	try:
		print ("GOT INTRO TRY")
		new_next_payment = apartment_object.payment_set.filter(paid=False).order_by("date_of_payment").first()
		new_next_payment_id = new_next_payment.id
		new_next_payment_date = new_next_payment.date_of_payment.strftime("%b. %d, %Y")
		context["new_next_payment_id"] = new_next_payment_id
		context["new_next_payment_date"] = new_next_payment_date
		context["new_next_payment_value"] = new_next_payment.fee_value
	except Exception as lol:
		print ("GOT INTO EXCEPCITON")
		print (lol)
		context["next_payment_error"] = True
	concept_of_income = "Cuota de Admon. Apto: {0}".format(payment.apartment.name)
	add_income_result = add_income(
		building=payment.apartment.building, 
		price=payment.fee_value, 
		concept=concept_of_income, 
		date=payment.payment_record_date)
	if add_income_result == True:
		pass
	else:
		context["error"] = True
	return render_to_json_response(context)

def mark_as_paid_inline_by_month(request, payment_id):
	if request.method == "POST":
		try:
			post_payment_record_date = request.POST.get("payment_record_date")
			post_fee_value = request.POST.get("fee_value")
			has_discount = request.POST.get("has_discount")
			building_id = request.POST.get("building_id")
			building = get_object_or_404(Building, id=building_id)
			building_wallet = building.buildingwallet
			discount_early_payment_amount = format_price(building_wallet.discount_early_payment_amount)
			date_format = datetime.datetime.strptime(post_payment_record_date, "%m/%d/%Y")
			valid_payment_record_date = datetime.date(year=date_format.year, month=date_format.month, day=date_format.day)
			valid_fee_value = format_price(post_fee_value)

			if has_discount == True:
				valid_fee_value = valid_fee_value - discount_early_payment_amount
			else:
				pass

			payment = get_object_or_404(Payment, id=payment_id)
			payment_fee_value = format_price(payment.fee_value)
			payment.payment_record_date = valid_payment_record_date
			payment.fee_value = valid_fee_value
			payment.paid = True
			payment.save()
		except Exception as lol:
			print ("Exception raised")
			print (lol)
	elif request.method == "GET":
		return HttpResponseForbidden()

	context = {}
	try:
		new_next_payment = Payment.get_next_by_date_of_payment(payment)
		new_next_payment_apartment_next = Payment.get_next_by_date_of_payment(new_next_payment).apartment.name
		context["new_next_payment_id"] = new_next_payment.id
		context["new_next_payment_apartment"] = new_next_payment.apartment.name
		try:
			owner = new_next_payment.apartment.get_owner()
			owner = "{0} {1}".format(owner.name, owner.last_name)
			context["new_next_payment_owner"] = owner
		except:
			context["new_next_payment_owner"] = None
		context["new_next_payment_value"] = new_next_payment.fee_value
		context["new_next_payment_apartment_next"] = new_next_payment_apartment_next
		context["discount_early_payment_amount"] = discount_early_payment_amount
	except Exception as lol:
		print (lol)
		context["next_payment_error"] = True

	building = payment.apartment.building
	building_wallet = building.buildingwallet
	concept_of_income = "Cuota de Admon. Apto: {0}".format(payment.apartment.name)
	try:
		add_income_result = add_income(
			cashflow=building_wallet.cashflow_for_wallet_payments, 
			price=payment.fee_value, 
			concept=concept_of_income, 
			date=payment.payment_record_date)
	except Exception as lol:
		print ("Exception raised")
		print (lol)

	if add_income_result == True:
		pass
	else:
		context["error"] = True
	return render_to_json_response(context)

def get_payment_data(payment):
	context = {}
	context["new_next_payment_id"] = payment.id
	context["new_next_payment_apartment"] = payment.apartment.name
	context["new_next_payment_value"] = payment.fee_value

	try:
		owner = apartment.get_owner()
		owner = "{0} {1}".format(owner.name, owner.last_name)
	except:
		owner = None
	context["new_next_payment_owner"] = owner
	return context

def get_payment(request, *args, **kwargs):
	payment_id = kwargs['payment_id']
	today = timezone.now()
	context = {}
	try:
		new_next_payment = Payment.objects.get(id=payment_id)
		context = get_payment_data(new_next_payment)
		new_next_payment_apartment_next = Payment.get_next_by_date_of_payment(new_next_payment).apartment.name
		context["new_next_payment_apartment_next"] = new_next_payment_apartment_next
		print (context)
	except Exception as lol:
		print (lol)
	return render_to_json_response(context)

def next_by_month(request, payment_id):
	payment = get_object_or_404(Payment, id=payment_id)
	new_next_payment = Payment.get_next_by_date_of_payment(payment)
	context = get_payment_data(new_next_payment)
	new_next_payment_apartment_next = Payment.get_next_by_date_of_payment(new_next_payment).apartment.name
	context["new_next_payment_apartment_next"] = new_next_payment_apartment_next
	return render_to_json_response(context)
