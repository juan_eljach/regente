from django import forms
from django.forms import ModelForm
from movements.models import CashFlow
from .models import BuildingWallet, Payment

class CreateWalletForm(ModelForm):
	has_discount_early_payment = forms.TypedChoiceField(coerce=lambda x: x == 'True', choices=((True, 'Si'), (False, 'No')), widget=forms.RadioSelect, required=False)

	class Meta:
		model = BuildingWallet
		fields = ["day_of_recurrent_payment", "has_discount_early_payment", "discount_early_payment_amount", "cashflow_for_wallet_payments"]

	def __init__(self, building, *args, **kwargs):
		super(CreateWalletForm, self).__init__(*args, **kwargs)
		self.fields['cashflow_for_wallet_payments'].queryset = CashFlow.objects.filter(building=building)