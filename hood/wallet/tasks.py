import datetime
from celery.decorators import task
from buildings.models import Building
from budgets.models import Budget
from dateutil.relativedelta import relativedelta
from celery.task.schedules import crontab
from celery.decorators import periodic_task

def format_price(price):
		price = price.replace(",","")
		price = price.replace(".","")
		price = int(price)

		return price

#crontab(minute='*/15')
#crontab(0, 0, day_of_month='1')
@periodic_task(run_every=(crontab(0, 0, day_of_month='1')), name="monthly_check_of_fees", ignore_result=True)
def pro():
	print ("lol")
	buildings = Building.objects.all()
	for building in buildings:
		budget = Budget.objects.filter(building=building).order_by("year").last()
		all_budget_items = budget.budgetitem_set.all()
		prices = [format_price(x.annual_quantity) for x in all_budget_items]
		total_price = 0
		for x in prices:
			total_price = total_price + x
		for apto in building.apartment_set.all():
			for payment in apto.payment_set.all():
				payment.fee_value = int(int(int(total_price) * float(apto.coefficient) / 100.0) / 12)
				payment.save()
			#traerme todos los payments del apto
			#setearle el price a cada paymen

@task(name="register_adminfees_task")
def register_fees(building_id, day_of_recurrent_payment, timestamp):
	building = Building.objects.get(id=building_id)
	apartments = building.apartment_set.all()
	timestamp_date = timestamp
	timestamp_date = datetime.datetime.strptime(timestamp, '%Y-%m-%d')
	timestamp_date = timestamp_date.date()
	today = datetime.date.today()

	day_of_recurrent_payment_date = datetime.date(year=today.year, month=today.month, day=day_of_recurrent_payment)

	from wallet.models import Payment

	if day_of_recurrent_payment_date > timestamp_date:
		for instance_apartment in apartments:
			print ("")
			Payment.objects.create(apartment=instance_apartment, date_of_payment=day_of_recurrent_payment_date)
			for x in range(1,12):
				Payment.objects.create(apartment=instance_apartment, date_of_payment=day_of_recurrent_payment_date + relativedelta(months=x))
	else:
		for instance_apartment in apartments:
			for x in range(1,13):
				Payment.objects.create(apartment=instance_apartment, date_of_payment=day_of_recurrent_payment_date + relativedelta(months=x))
	budget = Budget.objects.filter(building=building).order_by("year").last()
	all_budget_items = budget.budgetitem_set.all()
	prices = [format_price(x.annual_quantity) for x in all_budget_items]
	total_price = 0
	for x in prices:
		total_price = total_price + x
	for apto in building.apartment_set.all():
		for payment in apto.payment_set.all():
			payment.fee_value = int(int(int(total_price) * float(apto.coefficient) / 100.0) / 12)
			payment.save()




#	building = Building.objects.get(id=building_id)
#	building_wallet = BuildingWallet.objects.get(building=building)
#	timestamp = building_wallet.timestamp
#	today = datetime.date.today()
#	day_of_recurrent_payment = datetime.date(year=today.year, month=today.month, day=building_wallet.day_of_recurrent_payment)

#	if day_of_recurrent_payment > timestamp_datetime:
#		Payment.objects.create(apartment=instance_apartment, date_of_payment=day_of_recurrent_payment)
#		for x in range(1,12):
#			day_of_recurrent_payment = day_of_recurrent_payment + relativedelta(months=1)
#			Payment.objects.create(apartment=instance_apartment, date_of_payment=day_of_recurrent_payment)
#	else:
#		for x in range(1,13):
#			day_of_recurrent_payment = day_of_recurrent_payment + relativedelta(months=1)
#			Payment.objects.create(apartment=instance_apartment, date_of_payment=day_of_recurrent_payment)

#	all_budget_items = building.budgetitem_set.all()
#	prices = [int(x.annual_quantity.replace(".","")) for x in all_t]
#	total_price = 0
#	for x in prices:
#		total_price = total_price + x
#	all_properties = building.apartment_set.all()
#	for x in all_properties:
#		monthly_adminfee_price = int(int(int(total_price) * float(x.coeficient) / 100.0) / 12)