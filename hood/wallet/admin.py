from django.contrib import admin
from .models import AdministrationFee, Payment, BuildingWallet
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from properties.models import Apartment

class AdministrationFeeResource(resources.ModelResource):
	def before_import(self, dataset, dry_run=True, **kwargs):
		"""
		Make standard corrections to the dataset before displaying to user
		"""
		building_id = dataset.get_col(6)[0]
		del dataset["building"]

		i = 0
		last = dataset.height - 1
		while i <= last:
			try:
				apto = Apartment.objects.get(name=str(dataset.get_col(1)[0]), building=building_id)
			except Apartment.DoesNotExist:
				raise Exception("Apartment not in DB !")                   
			except Apartment.MultipleObjectsReturned:
				pass

			id_adminfee =  ''

			if apto:
				if (AdministrationFee.objects.filter(apartment=apto)):
					id_adminfee = AdministrationFee.objects.filter(apartment=apto)[0].id
				else:
					pass

			dataset.rpush((id_adminfee, 
				Apartment.objects.filter(name=str(dataset.get_col(1)[0]), building=building_id)[0].id,
				dataset.get_col(2)[0],
				dataset.get_col(3)[0],
				dataset.get_col(4)[0],
				dataset.get_col(5)[0]))
			dataset.lpop()
			i = i + 1

	class Meta:
		model = AdministrationFee

class AdministrationFeeAdmin(ImportExportModelAdmin):
	resource_class = AdministrationFeeResource
	pass

class PaymentAdmin(admin.ModelAdmin):
    list_display = ('apartment', '__str__', 'date_of_payment', 'paid',)
    search_fields = ['apartment__name']

admin.site.register(AdministrationFee, AdministrationFeeAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(BuildingWallet)
# Register your models here.
