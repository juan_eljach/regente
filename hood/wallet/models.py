import datetime
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from properties.models import Apartment
from datetime import datetime, timedelta
from buildings.models import Building
from movements.models import CashFlow
from .tasks import register_fees

class BuildingWallet(models.Model):
	building = models.OneToOneField(Building)
	day_of_recurrent_payment = models.IntegerField()
	has_discount_early_payment = models.BooleanField(default=False)
	discount_early_payment_amount = models.CharField(max_length=50, blank=True)
	cashflow_for_wallet_payments = models.ForeignKey(CashFlow, null=True, on_delete=models.SET_NULL)
	timestamp = models.DateField(auto_now_add=True)
	
	def __str__(self):
		return self.building.name

	def save(self, *args, **kwargs):
		if not self.pk:
			super(BuildingWallet, self).save(*args, **kwargs)
			register_fees.delay(self.building.id, self.day_of_recurrent_payment, self.timestamp.isoformat())
		else:
			super(BuildingWallet, self).save(*args, **kwargs)

#class ApartmentBalance(models.Model):
#	apartment = models.ForeignKey(Apartment, null=True, on_delete=models.SET_NULL)
#	advance = models.BigIntegerField(default=0)
#	debt = models.BigIntegerField(default=0)

#	def receipt():
#
#
#PEDIR DATE OF RECURRENT PAYMENT
#CALCULAR GENERACIÓN DE RECIBO EL 25 DE CADA MES

class AdministrationFee(models.Model):
	apartment = models.OneToOneField(Apartment)
	date_of_recurrent_payment = models.DateField()
	fee_life_starting = models.DateField()
	fee_life_ending = models.DateField()
	fee_value = models.IntegerField()

	def __str__(self):
		return self.apartment.name

#	def calculate_payments(self, pk, date_of_recurrent_payment, fee_life_starting, fee_life_ending):
#		one_month = timedelta(days=30)
#		administration_fee = AdministrationFee.objects.get(pk=pk)
#		date_of_recurrent_payment = str(date_of_recurrent_payment.month) + '/' + str(date_of_recurrent_payment.day) + '/' + str(date_of_recurrent_payment.year)
#		date_of_recurrent_payment = datetime.strptime(date_of_recurrent_payment, '%m/%d/%Y')
#		fee_life_starting = str(fee_life_starting.month) + '/' + str(fee_life_starting.day) + '/' + str(fee_life_starting.year)
#		fee_life_starting = datetime.strptime(fee_life_starting, '%m/%d/%Y')
#		fee_life_ending = str(fee_life_ending.month) + '/' + str(fee_life_ending.day) + '/' + str(fee_life_ending.year)
#		fee_life_ending = datetime.strptime(fee_life_ending, '%m/%d/%Y')
#		date_of_payment = date_of_recurrent_payment + one_month
		
#		while 1:
#			if date_of_payment < fee_life_ending:
#				Payment.objects.create(date_of_payment=date_of_payment, administration_fee=administration_fee)
#				date_of_payment = date_of_payment + one_month
#				continue
#			else:
#				break

#	def save(self, *args, **kwargs):
#		obj_created = False
#		if not self.pk:
#			super(AdministrationFee, self).save(*args, **kwargs)
#			obj_created = True
#		if obj_created:
#			self.calculate_payments(self.pk, self.date_of_recurrent_payment, self.fee_life_starting, self.fee_life_ending)


class Payment(models.Model):
	apartment = models.ForeignKey(Apartment, null=True, on_delete=models.SET_NULL)
	date_of_payment = models.DateField()
	paid = models.BooleanField(default=False)
	payment_record_date = models.DateField(null=True, blank=True)
	fee_value = models.CharField(max_length=50, blank=True)

	def get_next_payment(self):
		next_payment = Payment.get_next_by_date_of_payment(self)
		next_payment = next_payment.apartment
		return next_payment

	def __str__(self):
		return self.fee_value