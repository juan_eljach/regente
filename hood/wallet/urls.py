from django.conf.urls import patterns, url
from .views import (AdministrationFeeListView, AdministrationFeePaymentListView, 
	AdministrationFeePaymentsByMonth, next_by_month, mark_as_paid, mark_as_paid_inline, 
	mark_as_paid_inline_by_month, create_wallet, get_payment, get_payments_ajax)

urlpatterns = patterns('',
    url(r'^buildings/(?P<building_slug>[-\w]+)/wallet/$', AdministrationFeeListView.as_view(), name='wallet'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/wallet/monthly$', AdministrationFeePaymentsByMonth.as_view(), name='wallet_by_month'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/wallet/create$', create_wallet, name='create_wallet'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/wallet/(?P<apartment_pk>[-\w]+)/payments/$', AdministrationFeePaymentListView.as_view(), name='adminfee_payments'),
    url(r'^wallet/(?P<payment_id>[0-9]+)/paid/$', mark_as_paid, name="payment_paid"),
    url(r'^inlinewallet/(?P<payment_id>[0-9]+)/paid/$', mark_as_paid_inline, name="payment_paid_inline"),
    url(r'^walletbymonth/(?P<payment_id>[0-9]+)/paid/$', mark_as_paid_inline_by_month, name="wallet_by_month_paid"),
    url(r'^walletbymonth/(?P<payment_id>[0-9]+)/$', get_payment, name="get_payment"),
    url(r'^walletbymonth/(?P<payment_id>[0-9]+)/next/$', next_by_month, name="wallet_by_month_next"),
    url(r'^api/ajax_search/$', get_payments_ajax, name="autocomplete_payments_search"),
)