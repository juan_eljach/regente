from django import forms
from django.forms import ModelForm
from .models import Item

class AddItemForm(ModelForm):
    class Meta:
        model = Item
        fields = ["name", "description", "priority", "meeting"]
