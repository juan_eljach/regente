from django.shortcuts import render, get_object_or_404
from django.views.generic import CreateView, ListView, DetailView
from buildings.models import Building
from properties.models import AdministrativeCouncil
from .utils import render_to_json_response
from .forms import AddItemForm
from .models import Item, Meeting

class MeetingDetailView(DetailView):
	model = Meeting
	template_name = "meetns/meeting_detail.html"
	context_object_name = "meeting"

	def get_context_data(self, *args, **kwargs):
		context = super(MeetingDetailView, self).get_context_data(*args, **kwargs)
		obj = self.get_object()
		building_slug = self.kwargs['building_slug']
		building_object = Building.objects.get(slug__iexact=building_slug)
		context['building'] = building_object
		context['items'] = obj.item_set.all().order_by("id")
		return context

	def get_object(self, *args, **kwargs):
		meeting_pk = self.kwargs["meeting_pk"]
		obj = self.model.objects.get(pk=meeting_pk)
		return obj

class MeetingListView(ListView):
	model = Meeting
	template_name = "meetns/meeting_list.html"
	context_object_name = "meetings"

	def get_context_data(self, *args, **kwargs):
		context = super(MeetingListView, self).get_context_data(*args, **kwargs)
		building_slug = self.kwargs['building_slug']
		building_object = get_object_or_404(Building, slug__iexact=building_slug)
		try:
			admin_council = AdministrativeCouncil.objects.get(building=building_object)
		except:
			admin_council = None
		context['admin_council'] = admin_council
		context['building'] = building_object
		return context

	def get_queryset(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		building_object = Building.objects.get(slug__iexact=building_slug)
		return Meeting.objects.filter(building=building_object)

class AjaxFormResponseMixin(object):
	def form_invalid(self, form):
		return render_to_json_response(form.errors, status=400)

	def form_valid(self, form):
		self.object = form.save()
		context = {}
		return render_to_json_response(self.get_context_data(context))

class AjaxItemCreateView(AjaxFormResponseMixin, CreateView):
	form_class = AddItemForm

	def get_context_data(self, context):
		context['success'] = True
		context['name'] = self.object.name
		context['description'] = self.object.description
		context['done'] = self.object.done
		return context

def mark_as_done(request, item_id):
	item = get_object_or_404(Item, id=item_id)
	print (item)
	item.done = True
	item.save()
	context = {"done": item.done}
	return render_to_json_response(context)

def delete_item(request, item_id):
	item = get_object_or_404(Item, id=item_id)
	try:
		item.delete()
	except:
		pass
	context = {"deleted": True}
	return render_to_json_response(context)


