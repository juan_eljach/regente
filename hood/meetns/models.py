from django.db import models
from buildings.models import Building

class Meeting(models.Model):
	name = models.CharField(max_length=100)
	date = models.DateTimeField(blank=True)
	building = models.ForeignKey(Building)

	def __str__(self):
		return self.name

PRIORITY_CHOICES = (
	(1, 'Low'),
	(2, 'Normal'),
	(3, 'High'),
)

class Item(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField(max_length=1000)
	priority = models.IntegerField(choices=PRIORITY_CHOICES, default=0)
	done = models.BooleanField(default=False)
	created = models.DateTimeField(auto_now_add=True)
	meeting = models.ForeignKey(Meeting)

	def __str__(self):
		return self.name