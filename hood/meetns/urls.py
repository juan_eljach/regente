from django.conf.urls import patterns, url
from .views import AjaxItemCreateView, MeetingListView, MeetingDetailView, mark_as_done, delete_item

urlpatterns = patterns('',
    url(r'^buildings/(?P<building_slug>[-\w]+)/meetings/$', MeetingListView.as_view(), name="meetings"),
    url(r'^buildings/(?P<building_slug>[-\w]+)/meetings/(?P<meeting_pk>[-\w]+)/$', MeetingDetailView.as_view(), name="meeting_detail"),
    url(r'^item/add/$', AjaxItemCreateView.as_view(), name="item_add"),
    url(r'^meetings/(?P<item_id>[0-9]+)/done/$', mark_as_done, name="item_done"),
    url(r'^meetings/(?P<item_id>[0-9]+)/delete/$', delete_item, name="item_delete"),
)