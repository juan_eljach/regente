from django.contrib import admin
from .models import Item, Meeting

admin.site.register(Item)
admin.site.register(Meeting)
# Register your models here.
