import logging
from django.db import models
from accounts.models import UserProfile
from django.template import defaultfilters
from userena.utils import get_user_profile
from django.contrib.auth.models import User, Group, Permission
from accounts.models import UserProfile
from userena.utils import get_user_profile
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse_lazy
from .utils import SendEmail
from django.conf import settings

logger = logging.getLogger(__name__)

class Building(models.Model):
        building_administrator = models.ForeignKey(UserProfile, related_name="administrator", blank=True, null=True, on_delete=models.SET_NULL)
        building_watchman = models.ForeignKey(UserProfile, related_name="watchman", blank=True, null=True, on_delete=models.SET_NULL)
        address = models.CharField(max_length=100)
        name = models.CharField(max_length=100)
        phone_number =  models.CharField(max_length=100)
        slug = models.SlugField(max_length=500)

        def text_body(self, building_administrator, building_name, watchman_name, password, complaints_url, building_url):
            body="""
            <p>Hola <strong>{0}</strong></p>
            <p>Nos alegra mucho tenerte con nosotros, hemos terminado de registrar tu edificio: <strong>{1}</strong> al igual que creado una cuenta especial para el celador(a). Estas son las credenciales:</p>
            <p><strong>Usuario</strong>: {2}<strong> Contrase&ntilde;a</strong>: {3}</p>
            <p>Esta es una contrase&ntilde;a temporal, as&iacute; que por motivos de seguridad el celador(a) debe cambiarla cuando entre a la plataforma.</p>
            <p>Adem&aacute;s de todo esto, los habitantes del edificio pueden usar esta <strong>URL</strong>: {4} para registrar sus quejas o enviarte un mensaje.</p
            <p>Esperamos que disfrutes la experiencia de usar Regente.</p>
            <div style="text-align:center"><a href="{5}" style="-webkit-border-radius: 5; -moz-border-radius: 5; border-radius: 5px; font-family: Arial; color: #ffffff; font-size: 16px; background: #0f76ac; padding: 10px 10% 10px 10%; text-decoration: none;">Administrar mi edificio</a></div>
            """.format(
                str(building_administrator), 
                str(building_name), 
                str(watchman_name), 
                str(password), 
                str(complaints_url),
                str(building_url)
            )
            body = body.replace("á", "&aacute;")
            body = body.replace("é", "&eacute;")
            body = body.replace("í", "&iacute;")
            body = body.replace("ó", "&oacute;")
            body = body.replace("ú", "&uacute;")
            body = body.replace("ñ", "&ntilde;")
            return body

        def save(self, *args, **kwargs):
            self.slug = defaultfilters.slugify(self.name)
            building_name = self.name
            building_administrator = self.building_administrator.name
            administrator_email = self.building_administrator.user.email
            complaints_url = "http://www.regente.co/buildings/" + self.slug + "/complaints/create/"
            building_url = "http://www.regente.co/buildings/" + self.slug

            watchman_username = "celador%s" % building_name.lower().replace(" ", "")
            watchman_user, created = User.objects.get_or_create(username=watchman_username)
            password = User.objects.make_random_password(length=10)
            if created:
                watchman_user.set_password(password)
            group, created = Group.objects.get_or_create(name="Watchman")
            if created:
                content_type = ContentType.objects.get(app_label="properties", model="visit")
                perms = Permission.objects.filter(content_type=content_type)
                for perm in perms:
                    if "add" in perm.codename:
                        group.permissions.add(perm)
                    else:
                        pass
            watchman_user.groups.add(group)
            watchman_user.save()
            watchman_user_profile, created = UserProfile.objects.get_or_create(user=watchman_user, user_type="WTMN")
            self.building_watchman = watchman_user_profile
            watchman_name = watchman_user_profile.name

            try:
                super(Building, self).save(*args, **kwargs)
            except:
                if not settings.DEBUG:
                    logger.error("Error en la creacion del edificio cuando se intenta guardar el objeto en la base de datos", exc_info=True, extra={
                                'request': self.request,
                    })

            if not settings.DEBUG:
                SendEmail(
                    to_email=administrator_email,
                    to_emails=None,
                    to_name=building_administrator,
                    from_email="info@regente.co",
                    from_name="Regente",
                    subject="¡Tu edificio está listo!",
                    template_name="c9aae3e9-87fc-46e8-8d3a-3366d66dfc80",
                    html_text=self.text_body(building_administrator, building_name, watchman_username, password, complaints_url, building_url)
                )

        def get_first_building(self):
            userprofile = get_user_profile(user=self.request.user)
            first_building =self.model.objects.get(building_administrator=userprofile)[0]
            return first_building
                
        def __str__(self):
                return self.name