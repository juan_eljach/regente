from django.contrib import admin
from .models import Building

class BuildingAdmin(admin.ModelAdmin):
    exclude = ("slug", "building_watchman")

admin.site.register(Building, BuildingAdmin)
# Register your models here.
