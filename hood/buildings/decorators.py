from django.contrib.auth.models import User
from .models import Building
from accounts.models import UserProfile
from django.shortcuts import get_object_or_404
from functools import wraps
from django.http import HttpResponse

def must_be_yours(view_func):
    @wraps(view_func)
    def wrapper(request, *args, **kwargs):
        user = request.user
        building_slug = kwargs["building_slug"]
        building = Building.objects.get(slug__iexact=building_slug)
        user_profile = get_object_or_404(UserProfile, user=user)
        if user_profile.user_type == "ADMN":
            if not (building.building_administrator == user_profile): 
                return HttpResponse("It is not yours ! You are not permitted !",
                            content_type="application/json", status=403)
        elif user_profile.user_type == "WTMN":
            if not (building.building_watchman == user_profile): 
                return HttpResponse("It is not yours ! You are not permitted !",
                            content_type="application/json", status=403)
        return view_func(request, *args, **kwargs)
    return wrapper

