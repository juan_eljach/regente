import sendgrid

def SendEmail(to_email, to_emails, to_name, from_email, from_name, subject, template_name,
    html_text, text=None):
    sg = sendgrid.SendGridClient('regente', 'stereo82')
    message = sendgrid.Mail()
    if to_email:
        message.add_to(to_email)
    if to_name:
        message.add_to_name(to_name)
    if to_emails:
        message.smtpapi.set_tos(to_emails)
    message.set_from(from_email)
    message.set_from_name(from_name)
    message.set_subject(subject)
    if template_name:
        message.add_filter('templates', 'enabled', '1')
        message.add_filter('templates', 'template_id', template_name)
    if html_text:
        message.set_html(html_text)
    if text:
        message.set_text(text)
    try:
        sg.send(message)
    except Exception as msg_error:
        print (msg_error)