from django.conf.urls import patterns, url
from .views import LandingView, BuildingListView, BuildingDetailView, BuildingCreateView

urlpatterns = patterns('',
	url(r'^$', LandingView.as_view(), name="landing"),
    url(r'^buildings/$', BuildingListView.as_view(), name="buildings"),
    url(r'^buildings/create$', BuildingCreateView.as_view(), name="building_create"),
    url(r'^buildings/(?P<building_slug>[-\w]+)/$', BuildingDetailView.as_view(), name='building_detail')
)

