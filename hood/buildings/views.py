from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView, DetailView, TemplateView, CreateView
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from userena.utils import get_user_profile
from accounts.models import UserProfile
from .models import Building
from .decorators import must_be_yours

class LandingView(TemplateView):
	template_name = "landing.html"

class BuildingListView(ListView):
	model = Building
	template_name = "buildings/buildings.html"

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(BuildingListView, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(BuildingListView, self).get_context_data(**kwargs)
		userprofile = get_user_profile(user=self.request.user)
		buildings_by_user = None
		if userprofile.user_type == "ADMN":
			buildings_by_user = Building.objects.filter(building_administrator=userprofile)
		elif userprofile.user_type == "WTMN":
			buildings_by_user = Building.objects.filter(building_watchman=userprofile)
		context['userprofile'] = userprofile
		context['buildings'] = buildings_by_user
		context['userprofile'] = userprofile
		return context

class BuildingDetailView(DetailView):
	template_name = "buildings/building_detail.html"
	model = Building
	context_object_name = "building"

	@method_decorator(must_be_yours)
	def dispatch(self, *args, **kwargs):
		return super(BuildingDetailView, self).dispatch(*args, **kwargs)

	def get_object(self, *args, **kwargs):
		slug = self.kwargs['building_slug']
		obj = self.model.objects.get(slug__iexact=slug)
		return obj

	def get_context_data(self, *args, **kwargs):
		context = super(BuildingDetailView, self).get_context_data(*args, **kwargs)
		userprofile = get_user_profile(user=self.request.user)
		context["userprofile"] = userprofile
		return context

class BuildingCreateView(CreateView):
	template_name = "buildings/create_building.html"
	model = Building
	fields = ['address', 'name', 'phone_number']

	@method_decorator(permission_required("buildings.add_building", raise_exception=True))
	def dispatch(self, *args, **kwargs):
		return super(BuildingCreateView, self).dispatch(*args, **kwargs)

	def post(self, *args, **kwargs):
		user = self.request.user
		userprofile = get_object_or_404(UserProfile, user=user)
		form = self.get_form()
		if form.is_valid():
			building = form.save(commit=False)
			building.building_administrator = userprofile
			building.save()

			success_url = reverse('properties', kwargs={'building_slug': building.slug})
			return redirect(success_url)
		else:
			return self.render_to_response(self.get_context_data(form=form))


