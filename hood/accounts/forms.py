from django import forms
from django.utils.translation import ugettext_lazy as _

from userena.forms import SignupForm
from userena.utils import get_user_profile

from .models import AdministratorProfile
from django.contrib.auth.models import Group

PROFILE_TYPES = (
    (u'USER', 'User'),
    (u'ADMN', 'Admin'),
    (u'WTMN', 'Watchman'),
)

class SignupFormExtra(SignupForm):
    name = forms.CharField(label=_(u'Name'),max_length=30,required=False)
    last_name = forms.CharField(label=_(u'Last Name'),max_length=30,required=False)
    address = forms.CharField(label=_(u'Address'),max_length=30,required=False)
    phone_number = forms.CharField(label=_(u'Phone Number'),max_length=30,required=False)
    user_type = forms.ChoiceField(choices=PROFILE_TYPES, widget=forms.HiddenInput())

    def save(self):
        """
        Override the save method to save the first and last name to the user
        field.

        """
        # Original save method returns the user
        user = super(SignupFormExtra, self).save()

        # Get the profile, the `save` method above creates a profile for each
        # user because it calls the manager method `create_user`.
        # See: https://github.com/bread-and-pepper/django-userena/blob/master/userena/managers.py#L65
        user_profile = get_user_profile(user=user)

        # Be sure that you have validated these fields with `clean_` methods.
        # Garbage in, garbage out.
        user_profile.name = self.cleaned_data['name']
        user_profile.last_name = self.cleaned_data['last_name']
        user_profile.address = self.cleaned_data['address']
        user_profile.phone_number = self.cleaned_data['phone_number']
        user_profile.user_type = self.cleaned_data['user_type']

        if self.cleaned_data['user_type'] == "ADMN" and user:
            try:
                group = Group.objects.get(name="Administrator")
                user.groups.add(group)
            except:
                pass
        elif self.cleaned_data['user_type'] == "WTMN" and user:
            try:
                group = Group.objects.get(name="Watchman")
                user.groups.add(group)
            except:
                pass
        else:
            pass

        user.save()
        user_profile.save()
        return user