from django.db import models
from django.contrib.auth.models import User
from userena.models import UserenaBaseProfile

PROFILE_TYPES = (
    (u'USER', 'User'),
    (u'ADMN', 'Admin'),
    (u'WTMN', 'Watchman'),
)

class UserProfile(UserenaBaseProfile):
    user = models.OneToOneField(User, related_name="user_profile")
    address = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    name = models.CharField(max_length=100, blank=True)
    phone_number = models.CharField(max_length=100, blank=True)
    user_type = models.CharField(choices=PROFILE_TYPES, max_length=16)
	#some common fields here, which are shared among both administrator and individual profiles

    def is_administrator(self):
        if self.user_type == "ADMN":
            return True
        else:
            return False

    def is_watchman(self):
        if self.user_type == "WTMN":
            return True
        else:
            return False

class AdministratorProfile(models.Model):
    profile = models.ForeignKey(UserProfile)
    #administrator special fields here

    class Meta:
        db_table = 'administrator_profile'

#class IndividualUser(models.Model):
#    profile = models.ForeignKey(UserProfile)
    #Individual user fields here
#
#   class Meta:
#        db_table = 'individual_user'
