from django.conf.urls import patterns, url
from userena.views import signin
from .views import signup
from .forms import SignupFormExtra
urlpatterns = patterns('',
    url(r'^login/$', signin, {"template_name":"accounts/login.html",}, name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login', name="logout"),
    url(r'^signup/$', signup, {"template_name":"accounts/register.html", 'signup_form': SignupFormExtra}, name="signup"),
)

