from django.conf.urls import patterns, url
from .views import EmailListView, EmailWizard, EmailCreateView
#from .forms import EmailFormStepOne, EmailFormStepTwo

urlpatterns = patterns('',
    url(r'^buildings/(?P<building_slug>[-\w]+)/emails$', EmailListView.as_view(), name='emails'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/emails/create$', EmailCreateView.as_view(), name='create_email'),
)

