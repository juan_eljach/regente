from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView
from formtools.wizard.views import SessionWizardView
from buildings.models import Building
from .models import EmailToMany
from .forms import EmailForm
from .tasks import send_email

class EmailWizard(SessionWizardView):
    def done(self, form_list, **kwargs):
        slug = self.kwargs['building_slug']
        return redirect('emails', building_slug=slug)
    
    #def get_form_initial(self, "0"):
    #    return self.initial_dict.get("0", {"choices":})

    def get_context_data(self, *args, **kwargs):
        context = super(EmailWizard, self).get_context_data(*args, **kwargs)
        slug=self.kwargs['building_slug']
       
        return context

class EmailListView(ListView):
	model = EmailToMany
	template_name = "emails/emails_list.html"

#	@method_decorator(login_required)
#	def dispatch(self, *args, **kwargs):
#		return super(EmailListView, self).dispatch(*args, **kwargs)

	def get_context_data(self, *args, **kwargs):
		context = super(EmailListView, self).get_context_data(*args, **kwargs)
		building_slug = self.kwargs['building_slug']
		building_obj = Building.objects.get(slug__iexact=building_slug)
		context['building'] = building_obj
		return context

	def get_queryset(self, *args, **kwargs):
		slug = self.kwargs['building_slug']
		building_obj = Building.objects.get(slug__iexact=slug)
		queryset = self.model.objects.filter(building=building_obj)

class EmailCreateView(CreateView):
	form_class = EmailForm
	template_name = "emails/create_email.html"

	def get_context_data(self, *args, **kwargs):
		context = super(EmailCreateView, self).get_context_data(*args, **kwargs)
		building_slug = self.kwargs['building_slug']
		building_obj = Building.objects.get(slug__iexact=building_slug)
		context['building'] = building_obj
		return context

	def form_valid(self, form, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		building_obj = Building.objects.get(slug__iexact=building_slug)
		new_email = form.save(commit=False)
		new_email.building = building_obj
		new_email.save()
		to_list = form.cleaned_data['to_emails']
		new_email_id = new_email.id
		send_email(new_email_id, to_list)
		return HttpResponse("TODO CAUCHOOO PERRITOOOOO")