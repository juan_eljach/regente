from django import forms
from django.forms import ModelForm
from .models import EmailToMany

RECEPIENTS_CHOICES = (
	('owners', 'Propietarios'),
	('inhabitant_owners', 'Propietarios que son residentes'),
	('owners_with_pet', 'Propietarios con mascota'),
	('inhabitants', 'Residentes'),
)

class EmailForm(ModelForm):
	to_emails = forms.ChoiceField(choices=RECEPIENTS_CHOICES)
	class Meta:
		model = EmailToMany
		fields = ["subject", "to_emails", "from_name", "text"]