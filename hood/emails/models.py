import ast
from django.db import models
from buildings.models import Building

class ListField(models.TextField):
    __metaclass__ = models.SubfieldBase
    description = "Stores a python list"

    def __init__(self, *args, **kwargs):
        super(ListField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if not value:
            value = []

        if isinstance(value, list):
            return value

        return ast.literal_eval(value)

    def get_prep_value(self, value):
        if value is None:
            return value

        return str(value)

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_db_prep_value(value)

class EmailToMany(models.Model):
    building = models.ForeignKey(Building)
    subject = models.CharField(max_length=255)
    to_emails = models.CharField(max_length=255)
    from_email = models.EmailField(max_length=255, blank=False, default="info@regente.co")
    from_name = models.CharField(max_length=255, blank=True, null=True, default="Regente")
    sent = models.BooleanField(default=False)
    template_name = models.CharField(max_length=255, blank=True, null=True)
    text = models.TextField(max_length=5000, blank=True, null=True)
    html_text = models.TextField(max_length=5000, blank=True, null=True)
    date_send = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s' % self.subject

class EmailToSingle(models.Model):
    subject = models.CharField(max_length=255)
    to_email = models.EmailField(max_length=255, blank=False)
    to_name = models.CharField(max_length=255, blank=True)
    from_email = models.EmailField(max_length=255, blank=False)
    from_name = models.CharField(max_length=255)
    sent = models.BooleanField(default=False)
    template_name = models.CharField(max_length=255, blank=True, null=True)
    text = models.TextField(max_length=5000, blank=True, null=True)
    html_text = models.TextField(max_length=5000, blank=True, null=True)
    date_send = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s' % self.to_email