from __future__ import absolute_import
from celery import shared_task
from celery.task.schedules import crontab
from celery.decorators import periodic_task
from properties.models import Apartment
from buildings.utils import SendEmail
from .models import EmailToMany

#@shared_task
def send_email(email_id, to_list):
	try:
		email_object = EmailToMany.objects.get(id=email_id)
		print (email_object)
		building_object = email_object.building
		params = dict(
			subject=email_object.subject,
			to_email=None,
			to_name=None,
			from_email=email_object.from_email,
			from_name=email_object.from_name,
			template_name=email_object.template_name,
			text=email_object.text,
			html_text=email_object.html_text
		)
		if to_list == "owners":
			print ("Entrando a la opcion owners")
			apartments = Apartment.objects.filter(building=building_object)
			owners = []
			for apartment in apartments:
				owners_by_apto = apartment.owner_set.all()
				for owner in owners_by_apto:
					if not owner in owners:
						owners.append(owner)
			emails = [owner.email for owner in owners]
			params["to_emails"] = emails
			SendEmail(**params)
		elif to_list == "inhabitant_owners":
			apartments = Apartment.objects.filter(building=building_object)
			inhabitant_owners = []
			for apartment in apartments:
				inhabitant_owners_by_apto = apartment.owner_set.all()
				for inhabitant in inhabitant_owners_by_apto:
					if not inhabitant in inhabitant_owners and inhabitant.is_inhabitant == True:
						inhabitant_owners.append(inhabitant)
			emails = [inhabitant.email for inhabitant in inhabitant_owners]
		elif to_list == "owners_with_pet":
			apartments = Apartment.objects.filter(building=building_object)
			owners_with_pet = []
			for apartment in apartments:
				owners_by_apto = apartment.owner_set.all()
				for owner in owners_by_apto:
					if not owner in owners_with_pet and owner.has_pet == True:
						owners_with_pet.append(owner)
			emails = [owner.email for owner in owners_with_pet]
			params["to_emails"] = emails
			SendEmail(**params)
		elif to_list == "inhabitants":
			apartments = Apartment.objects.filter(building=building_object)
			inhabitants = []
			for apartment in apartments:
				for inhabitant in apartment.inhabitant_set.all():
					if not inhabitant in inhabitants:
						inhabitants.append(inhabitant)
			emails = [inhabitant.email for inhabitant in inhabitants]
			params["to_emails"] = emails
			SendEmail(**params)
		else:
			pass
	except Exception as e:
		print ("There was an error: {0}".format(e))