from django import forms
from django.forms import ModelForm, formset_factory
from .models import Provider, BudgetItem

class BudgetItemForm(ModelForm):
	concept = forms.CharField(widget=forms.TextInput(attrs={'class': 'formset-input form-control', 'style':'width: 300px;'}), label='Concepto')
	monthly_quantity = forms.CharField(widget=forms.TextInput(attrs={'class': 'formset-input form-control', 'placeholder': '200.000'}), label='Precio Mensual')
	annual_quantity = forms.CharField(widget=forms.TextInput(attrs={'class': 'formset-input form-control', 'placeholder': '2.400.000'}), label='Precio Anual')
	budget = forms.CharField(widget=forms.HiddenInput(), label='')

	class Meta:
		model = BudgetItem
		fields = ['concept', 'monthly_quantity', 'annual_quantity']

class AddProviderForm(ModelForm):
    class Meta:
        model = Provider
        fields = ["name", "address", "service", "email", "phone_number", "bank_account_number"]

