from django.db import models
from buildings.models import Building
from datetime import datetime, timedelta

class Provider(models.Model):
	address = models.CharField(max_length=100)
	bank_account_number = models.CharField(max_length=100, blank=True)
	email = models.EmailField(max_length=254)
	name =  models.CharField(max_length=100)
	phone_number = models.CharField(max_length=100)
	service =  models.CharField(max_length=100)

	def __str__(self):
		return self.name

FRECUENCY_CHOICES = (
	(7, 'Semanal'),
	(15, 'Quincenal'),
	(30, 'Mensual'),
	(91, 'Trimestral'),
	(182, 'Semestral'),
	(365, 'Anual'),
)

class Budget(models.Model):
	building = models.ForeignKey(Building)
	total_price = models.IntegerField(null=True, blank=True)
	year = models.IntegerField()

	def __str__(self):
		return self.building.name

class BudgetItem(models.Model):
	budget = models.ForeignKey(Budget)
	concept = models.CharField(max_length=250)
	monthly_quantity = models.CharField(max_length=250, blank=True)
	annual_quantity = models.CharField(max_length=250, blank=True)

	def __str__(self):
		return self.concept


	#def calculate_payments(self, pk, period, budget_life_starting, budget_life_ending):
	#	period = timedelta(days=period)
	#	budget = Budget.objects.get(pk=pk)
	#	life_starting = str(budget_life_starting.month) + '/' + str(budget_life_starting.day) + '/' + str(budget_life_starting.year)
	#	life_starting = datetime.strptime(life_starting, '%m/%d/%Y')
	#	life_ending = str(budget_life_ending.month) + '/' + str(budget_life_ending.day) + '/' + str(budget_life_ending.year)
	#	life_ending = datetime.strptime(life_ending, '%m/%d/%Y')
	#	date_of_payment = life_starting + period

	#	while True:
	#		if date_of_payment < life_ending:
	#			BudgetPayment.objects.create(date_of_payment=date_of_payment, budget=budget)
	#			date_of_payment = date_of_payment + period
	#			continue
	#		else:
	#			break

	#def calculate_total_price(self, period, budget_life_starting, budget_life_ending, unit_price):
	#	period = timedelta(days=period)
	#	life_starting = str(budget_life_starting.month) + '/' + str(budget_life_starting.day) + '/' + str(budget_life_starting.year)
	#	life_starting = datetime.strptime(life_starting, '%m/%d/%Y')
	#	life_ending = str(budget_life_ending.month) + '/' + str(budget_life_ending.day) + '/' + str(budget_life_ending.year)
	#	life_ending = datetime.strptime(life_ending, '%m/%d/%Y')
	#	date_of_payment = life_starting + period
	#	payments_counter = 0

	#	while True:
	#		if date_of_payment < life_ending:
	#			payments_counter += 1
	#			date_of_payment = date_of_payment + period
	#			continue
	#		else:
	#			break
	#	total_price = abs(int(payments_counter)) * abs(int(unit_price))
	#	return total_price

	#def save(self, *args, **kwargs):
	#	obj_created = False
	#	if not self.pk:
	#		self.total_price = self.calculate_total_price(self.frecuency, self.budget_life_starting, self.budget_life_ending, self.unit_price)
	#		super(Budget, self).save(*args, **kwargs)
	#		obj_created = True
	#	if obj_created:
	#		self.calculate_payments(self.pk, self.frecuency, self.budget_life_starting, self.budget_life_ending)


class BudgetPayment(models.Model):
	date_of_payment = models.DateField()
	budget = models.ForeignKey(Budget)
	paid = models.BooleanField(default=False)

	def __str__(self):
		return self.budget.item