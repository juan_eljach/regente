import tempfile
import logging
import os
from django.shortcuts import render, render_to_response
from django.views.generic import ListView, CreateView, UpdateView, View, TemplateView
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse
from django.utils.decorators import method_decorator
from django.utils.encoding import force_text
from django.contrib.contenttypes.models import ContentType
from django.contrib import messages
from django.forms import formset_factory
from django.http import HttpResponseRedirect
from django.conf import settings
from django.forms.models import construct_instance
from formtools.wizard.views import SessionWizardView
from import_export.forms import ImportForm
from import_export.forms import ConfirmImportForm
from import_export.formats import base_formats
from import_export.resources import modelresource_factory
from import_export import resources
from buildings.models import Building
from meetns.utils import render_to_json_response
from properties.models import Apartment
from .models import Budget, BudgetPayment, BudgetItem
from .forms import AddProviderForm, BudgetItemForm

try:
	mixpanel = settings.MIXPANEL
except:
	mixpanel = False

logger = logging.getLogger(__name__)

#Servicios Profesionales
default_concepts_step_one = [
	{'concept':'Administrador', 'category':'servicios profesionales'},
	{'concept':'Revisor Fiscal', 'category':'servicios profesionales'},
	{'concept':'Asesoria Legal', 'category':'servicios profesionales'},
]

#Servicios Generales
default_concepts_step_two = [
	{'concept':'Seguridad Privada', 'category':'servicios'},
	{'concept':'Aseo - Servicios generales', 'category':'servicios'},
	{'concept':'Salvavidas', 'category':'servicios'},
	{'concept':'Servicio de Energia', 'category':'servicios'},
	{'concept':'Servicio Telefonico', 'category':'servicios'},
	{'concept':'Jardinería y Abonos', 'category':'servicios'},
]

#Utencilios
default_concepts_step_three = [
	{'concept':'Elementos de Aseo y cafeteria', 'category':'utencilios'},
	{'concept':'Correo, Papelería y fotocopias', 'category':'utencilios'},
	{'concept':'Bombillos y Flores', 'category':'utencilios'},
]

#Mantenimientos
default_concepts_step_four = [
	{'concept':'Mantenimiento de ascensores', 'category':'mantenimientos'},
	{'concept':'Mantenimientos Preventivos Motobombas', 'category':'mantenimientos'},
	{'concept':'Mantenimiento Piscina - Implementos', 'category':'mantenimientos'},
	{'concept':'Mantenimiento circuito C.C.T.V', 'category':'mantenimientos'},
	{'concept':'Mantenimiento Porton Electrico', 'category':'mantenimientos'},
	{'concept':'Mantenimiento Citofonos/Telefono', 'category':'mantenimientos'},
	{'concept':'Mantenimiento Anual Planta Elec.', 'category':'mantenimientos'},
	{'concept':'Mantenimiento y Reparaciones Locativas Imprevistos', 'category':'mantenimientos'},
	{'concept':'Mantenimiento de Extintores', 'category':'mantenimientos'},
]

default_concepts = [
	{'concept':'Mantenimiento de ascensores', 'category':'mantenimientos'},
	{'concept':'Mantenimientos Preventivos Motobombas', 'category':'mantenimientos'},
	{'concept':'Mantenimiento Piscina - Implementos', 'category':'mantenimientos'},
	{'concept':'Mantenimiento circuito C.C.T.V', 'category':'mantenimientos'},
	{'concept':'Mantenimiento Porton Electrico', 'category':'mantenimientos'},
	{'concept':'Mantenimiento Citofonos/Telefono', 'category':'mantenimientos'},
	{'concept':'Mantenimiento Anual Planta Elec.', 'category':'mantenimientos'},
	{'concept':'Mantenimiento y Reparaciones Locativas Imprevistos', 'category':'mantenimientos'},
	{'concept':'Mantenimiento de Extintores', 'category':'mantenimientos'},
]

#otros
default_concepts_step_five = [
		{'concept':'Transportes, Taxis y acarreos', 'category':'otros'},
		{'concept':'Pruebas laboratorio Piscina', 'category':'otros'},
		{'concept':'Gastos Bancarios - Impuestos asumidos', 'category':'otros'},
		{'concept':'Compra de camaras-monitores-ventiladores', 'category':'otros'},
	]

FORMS = [
	("servicios-profesionales", formset_factory(BudgetItemForm, extra=0)),
	("servicios-generales", formset_factory(BudgetItemForm, extra=0)),
	("utencilios", formset_factory(BudgetItemForm, extra=0)),
	("mantenimientos", formset_factory(BudgetItemForm, extra=0)),
	("otros", formset_factory(BudgetItemForm, extra=2)),
]

class BudgetItemsWizard(SessionWizardView):
	template_name = "budgets/manage_budgetitems.html"

	def done(self, form_list, **kwargs):
		building_slug = self.kwargs['building_slug']
		for formset in form_list:
			if formset.is_valid():
				for form in formset:
					concept = form.cleaned_data.get('concept')
					monthly_quantity = form.cleaned_data.get('monthly_quantity')
					annual_quantity = form.cleaned_data.get('annual_quantity')
					budget = form.cleaned_data.get('budget')
					try:
						budget_instance = Budget.objects.get(id=budget, building__slug__iexact=building_slug)
					except:
						if not settings.DEBUG:
							loggcer.error("Error en formwizard de creacion de los items de presupuesto: El presupuesto no se habia creado", exc_info=True, extra={
                                'request': self.request,
                            })
					try:                    
						new_budgetitem = BudgetItem.objects.create(concept= concept, monthly_quantity= monthly_quantity, annual_quantity= annual_quantity, budget = budget_instance)
						new_budgetitem.save()
					except:
						pass
			else:
				if not settings.DEBUG:
					logger.error("Error en la validacion de un formset en el formwizard de creacion de presupuesto", exc_info=True, extra={
                        'request': self.request,
                    })
				print ("THERE WAS A BIG FUCKING ERROR")
				print (formset.errors)

		if mixpanel:
			mixpanel.track(self.request.user.id, 'Registro de presupuesto completado')
			mixpanel.track(self.request.user.id, 'Registro de presupuesto de forma manual en Onboarding')

		success_url = reverse('createcashflows', kwargs={'building_slug': building_slug})
		return redirect(success_url)

	def get_form_initial(self, step, **kwargs):
		building_slug = self.kwargs['building_slug']
		budget_id = self.kwargs['budget_id']
		budget = Budget.objects.get(id=budget_id, building__slug__iexact=building_slug)
		init_dict = {}
		if step == 'servicios-profesionales':
			init_dict= [{'concept':x['concept'], 'category':x['category'], 'budget':budget.id} for x in default_concepts_step_one]
		elif step == 'servicios-generales':
			init_dict= [{'concept':x['concept'], 'category':x['category'], 'budget':budget.id} for x in default_concepts_step_two]
		elif step == 'utencilios':
			init_dict= [{'concept':x['concept'], 'category':x['category'], 'budget':budget.id} for x in default_concepts_step_three]
		elif step == 'mantenimientos':
			init_dict= [{'concept':x['concept'], 'category':x['category'], 'budget':budget.id} for x in default_concepts_step_four]
		elif step == 'otros':
			init_dict= [{'concept':x['concept'], 'category':x['category'], 'budget':budget.id} for x in default_concepts_step_five]
		else:
			pass
		return init_dict

class BudgetListView(ListView):
	template_name = "budgets/budgets.html"
	model = Budget
	context_object_name = "budgets"

	def get_context_data(self, *args, **kwargs):
		context = super(BudgetListView, self).get_context_data(**kwargs)
		building_slug = self.kwargs['building_slug']
		building_object = Building.objects.get(slug__iexact=building_slug)
		budgets_in_building = self.model.objects.filter(building=building_object)
		context['building'] = building_object
		context['budgets'] = budgets_in_building
		context['building'] = building_object
		return context

def manage_budgetitems(request):
	BudgetItemFormSet = formset_factory(BudgetItemForm, extra=3)
	if request.method == "POST":
		formset = BudgetItemFormSet(request.POST, request.FILES)
		if formset.is_valid():
			pass
		else:
			pass
	else:
		formset = BudgetItemFormSet(initial=[{'concept':x['concept'], 'category':x['category']} for x in default_concepts])
	return render_to_response('budgets/manage_budgetitems.html', {'formset':formset}, context_instance=RequestContext(request))
	

class BudgetItemResource(resources.ModelResource):

    def before_import(self, dataset, dry_run=True, **kwargs):
        """
        Make standard corrections to the dataset before displaying to user
        """
        budget=kwargs['budget']
        dataset.append_col([None for row in range(len(dataset))], header='id')
        dataset.append_col([budget.id for row in range(len(dataset))], header='budget')

    class Meta:
        model = BudgetItem
        fields = ('id', 'budget', 'concept', 'monthly_quantity', 'annual_quantity')

class BudgetItemImport(View):
    model = BudgetItem
    from_encoding = "utf-8"

    #: import / export formats
    DEFAULT_FORMATS = (
        base_formats.CSV,
        base_formats.XLS,
        base_formats.TSV,
        base_formats.ODS,
        base_formats.JSON,
        base_formats.YAML,
        base_formats.HTML,
    )
    formats = DEFAULT_FORMATS
    #: template for import view
    import_template_name = 'budgets/import.html'
    resource_class = BudgetItemResource

    def get_import_formats(self):
        """
        Returns available import formats.
        """
        return [f for f in self.formats if f().can_import()]

    def get_resource_class(self):
        if not self.resource_class:
            return modelresource_factory(self.model)
        else:
            return self.resource_class

    def get_import_resource_class(self):
        """
        Returns ResourceClass to use for import.
        """
        return self.get_resource_class()

    def get(self, *args, **kwargs ):
        '''
        Perform a dry_run of the import to make sure the import will not
        result in errors.  If there where no error, save the user
        uploaded file to a local temp file that will be used by
        'process_import' for the actual import.
        '''
        resource = self.get_import_resource_class()()
        building_slug = self.kwargs['building_slug']
        building_object = get_object_or_404(Building, slug__iexact=building_slug)
        budget_object = Budget.objects.filter(building=building_object).order_by("year").last()

        context = {}
        context['building'] = building_object
        context['budget'] = budget_object

        import_formats = self.get_import_formats()
        form = ImportForm(import_formats,
                          self.request.POST or None,
                          self.request.FILES or None)

        if self.request.POST and form.is_valid():
            input_format = import_formats[
                int(form.cleaned_data['input_format'])
            ]()
            import_file = form.cleaned_data['import_file']
            # first always write the uploaded file to disk as it may be a
            # memory file or else based on settings upload handlers
            with tempfile.NamedTemporaryFile(delete=False) as uploaded_file:
                for chunk in import_file.chunks():
                    uploaded_file.write(chunk)

            # then read the file, using the proper format-specific mode
            with open(uploaded_file.name,
                      input_format.get_read_mode()) as uploaded_import_file:
                # warning, big files may exceed memory
                data = uploaded_import_file.read()
                if not input_format.is_binary() and self.from_encoding:
                    data = force_text(data, self.from_encoding)
                dataset = input_format.create_dataset(data)
                result = resource.import_data(dataset, dry_run=True,
                                              raise_errors=False, budget=budget_object)

            context['result'] = result

            if not result.has_errors():
                context['confirm_form'] = ConfirmImportForm(initial={
                    'import_file_name': os.path.basename(uploaded_file.name),
                    'original_file_name': uploaded_file.name,
                    'input_format': form.cleaned_data['input_format'],
                })

        context['form'] = form
        context['opts'] = self.model._meta
        context['fields'] = [f.column_name for f in resource.get_fields()]

        return TemplateResponse(self.request, [self.import_template_name], context)


    def post(self, *args, **kwargs ):
        '''
        Perform a dry_run of the import to make sure the import will not
        result in errors.  If there where no error, save the user
        uploaded file to a local temp file that will be used by
        'process_import' for the actual import.
        '''
        resource = self.get_import_resource_class()()
        building_slug = self.kwargs['building_slug']
        building_object = get_object_or_404(Building, slug__iexact=building_slug)
        budget_object = Budget.objects.filter(building=building_object).order_by("year").last()

        context = {}
        context['building'] = building_object
        context['budget'] = budget_object

        import_formats = self.get_import_formats()
        form = ImportForm(import_formats,
                          self.request.POST or None,
                          self.request.FILES or None)

        if self.request.POST and form.is_valid():
            input_format = import_formats[
                int(form.cleaned_data['input_format'])
            ]()
            import_file = form.cleaned_data['import_file']
            # first always write the uploaded file to disk as it may be a
            # memory file or else based on settings upload handlers
            with tempfile.NamedTemporaryFile(delete=False) as uploaded_file:
                for chunk in import_file.chunks():
                    uploaded_file.write(chunk)

            # then read the file, using the proper format-specific mode
            with open(uploaded_file.name,
                      input_format.get_read_mode()) as uploaded_import_file:
                # warning, big files may exceed memory
                data = uploaded_import_file.read()
                if not input_format.is_binary() and self.from_encoding:
                    data = force_text(data, self.from_encoding)
                dataset = input_format.create_dataset(data)
                result = resource.import_data(dataset, dry_run=True,
                                              raise_errors=False, budget=budget_object)

            context['result'] = result

            if not result.has_errors():
                context['confirm_form'] = ConfirmImportForm(initial={
                    'import_file_name': os.path.basename(uploaded_file.name),
                    'original_file_name': uploaded_file.name,
                    'input_format': form.cleaned_data['input_format'],
                })

        context['form'] = form
        context['opts'] = self.model._meta
        context['fields'] = [f.column_name for f in resource.get_fields()]

        return TemplateResponse(self.request, [self.import_template_name], context)

class BudgetItemProcessImport(View):
    model = BudgetItem
    from_encoding = "utf-8"

    #: import / export formats
    DEFAULT_FORMATS = (
        base_formats.CSV,
        base_formats.XLS,
        base_formats.TSV,
        base_formats.ODS,
        base_formats.JSON,
        base_formats.YAML,
        base_formats.HTML,
    )
    formats = DEFAULT_FORMATS
    #: template for import view
    import_template_name = 'budgets/import.html'
    resource_class = BudgetItemResource

    def get_import_formats(self):
        """
        Returns available import formats.
        """
        return [f for f in self.formats if f().can_import()]

    def get_resource_class(self):
        if not self.resource_class:
            return modelresource_factory(self.model)
        else:
            return self.resource_class

    def get_import_resource_class(self):
        """
        Returns ResourceClass to use for import.
        """
        return self.get_resource_class()

    def post(self, *args, **kwargs):
        '''
        Perform the actual import action (after the user has confirmed he
    wishes to import)
        '''
        opts = self.model._meta
        resource = self.get_import_resource_class()()
        building_slug = self.kwargs['building_slug']
        building_object = get_object_or_404(Building, slug__iexact=building_slug)
        budget_object = Budget.objects.filter(building=building_object).order_by("year").last()

        confirm_form = ConfirmImportForm(self.request.POST)
        if confirm_form.is_valid():
            import_formats = self.get_import_formats()
            input_format = import_formats[
                int(confirm_form.cleaned_data['input_format'])
            ]()
            import_file_name = os.path.join(
                tempfile.gettempdir(),
                confirm_form.cleaned_data['import_file_name']
            )
            import_file = open(import_file_name, input_format.get_read_mode())
            data = import_file.read()
            if not input_format.is_binary() and self.from_encoding:
                data = force_text(data, self.from_encoding)
            dataset = input_format.create_dataset(data)
            result = resource.import_data(dataset, dry_run=False,
                                 raise_errors=True, budget=budget_object)

            success_message = ('Import finished')
            messages.success(self.request, success_message)
            import_file.close()
            
            return redirect('createcashflows', building_slug=building_slug)
        else:
            print (confirm_form.errors)


class BudgetCreateView(CreateView):
	template_name = "budgets/create_budget.html"
	model = Budget
	fields = ['year']

	@method_decorator(permission_required("budgets.add_budget", raise_exception=True))
	def dispatch(self, *args, **kwargs):
		return super(BudgetCreateView, self).dispatch(*args, **kwargs)

	def get_success_url(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		budget_id = kwargs['budget_id']
		return reverse('budget_select_import', kwargs={'building_slug': building_slug, 'budget_id': budget_id})

	def get_context_data(self, **kwargs):
		context = super(BudgetCreateView, self).get_context_data(**kwargs)
		return context

	def form_valid(self, form, **kwargs):
		building_slug = self.kwargs['building_slug']
		building_object = Building.objects.get(slug__iexact=building_slug)
		budget = form.save(commit=False)
		budget.building = building_object
		budget.save()

		success_url = self.get_success_url(budget_id=budget.id)
		return redirect(success_url)

	def form_invalid(self, form):
		print ("There was an error")
		return self.render_to_response(self.get_context_data(form=form))

#	def post(self, *args, **kwargs):
#		building_slug = self.kwargs['building_slug']
#		building_object = Building.objects.get(slug__iexact=building_slug)
#		form = self.get_form()
#		if form.is_valid():
#			budget = form.save(commit=False)
#			budget.building = building_object
#			budget.save()
#
#			success_url = self.get_success_url()
#			return redirect(success_url)
#		else:
#			return self.render_to_response(self.get_context_data(form=form))


class BudgetSelectImport(TemplateView):
    template_name = "budgets/budget_select_import.html"

    def get_context_data(self, **kwargs):
        context = super(BudgetSelectImport, self).get_context_data(**kwargs)
        building_slug = self.kwargs["building_slug"]
        budget_id = self.kwargs["budget_id"]
        building_object = get_object_or_404(Building, slug__iexact=building_slug)
        budget_object = get_object_or_404(Budget, id=budget_id)
        context["building"] = building_object
        context["budget"] = budget_object
        return context

class BudgetUpdateView(UpdateView):
	template_name = "budgets/edit_budget.html"
	model = Budget
	fields = ['item', 'provider', 'quantity', 'unit', 'unit_price', 'budget_life_starting', 'budget_life_ending']

	def get_context_data(self, **kwargs):
		context = super(BudgetUpdateView, self).get_context_data(**kwargs)
		building_slug = self.kwargs['building_slug']
		building_object = Building.objects.get(slug__iexact=building_slug)
		context['building'] = building_object
		return context

	def get_object(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		budget_pk = self.kwargs['budget_pk']
		building_object = Building.objects.get(slug__iexact=building_slug)
		obj = self.model.objects.get(pk=budget_pk, building=building_object)
		return obj

	def get_success_url(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		budget_pk = self.kwargs['budget_pk']
		return reverse('budgets', kwargs={'building_slug': building_slug})

class BudgetPaymentListView(ListView):
	model = BudgetPayment
	budget_model = Budget
	template_name = "budgets/payments_list.html"

	def get_object(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		budget_pk = self.kwargs['budget_pk']
		building_object = Building.objects.get(slug__iexact=building_slug)
		obj = self.budget_model.objects.get(pk=budget_pk, building=building_object)
		return obj

	def get_context_data(self, *args, **kwargs):
		context = super(BudgetPaymentListView, self).get_context_data(*args, **kwargs)
		budget_obj = self.get_object()
		payments = budget_obj.budgetpayment_set.all().order_by("date_of_payment")
		building_slug = self.kwargs['building_slug']
		building_object = Building.objects.get(slug__iexact=building_slug)
		context['building'] = building_object
		context['budget'] = budget_obj
		context['payments'] = payments
		return context

class AjaxFormResponseMixin(object):
	def form_invalid(self, form):
		print ("Form invalidad")
		return render_to_json_response(form.errors, status=400)

	def form_valid(self, form):
		print ("Form validad")
		self.object = form.save()
		print ("Form saved")
		context = {}
		return render_to_json_response(self.get_context_data(context))

class AjaxProviderCreateView(AjaxFormResponseMixin, CreateView):
	form_class = AddProviderForm

	def get_context_data(self, context):
		context['id'] = self.object.id
		context['name'] = self.object.name
		return context