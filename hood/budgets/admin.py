from django.contrib import admin
from .models import Budget, Provider, BudgetPayment, BudgetItem

class BudgetAdmin(admin.ModelAdmin):
    exclude = ("total_price",)

class BudgetPaymentAdmin(admin.ModelAdmin):
    list_display = ("__str__", "date_of_payment")

admin.site.register(Budget, BudgetAdmin)
admin.site.register(Provider)
admin.site.register(BudgetPayment, BudgetPaymentAdmin)
admin.site.register(BudgetItem)