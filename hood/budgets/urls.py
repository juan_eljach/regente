from django.conf.urls import patterns, url
from .views import BudgetListView, BudgetCreateView, BudgetUpdateView, BudgetPaymentListView, AjaxProviderCreateView, BudgetItemsWizard, FORMS, manage_budgetitems, BudgetItemImport, BudgetItemProcessImport, BudgetSelectImport
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('',
    url(r'^buildings/(?P<building_slug>[-\w]+)/budgets/$', BudgetListView.as_view(), name='budgets'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/budgets/create$', BudgetCreateView.as_view(), name='create_budget'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/budgets/(?P<budget_id>[0-9]+)/select$', BudgetSelectImport.as_view(), name='budget_select_import'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/budgets/(?P<budget_id>[0-9]+)/items/create$', BudgetItemsWizard.as_view(FORMS), name='create_budgetitems'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/budgets/(?P<budget_pk>[-\w]+)/edit/$', BudgetUpdateView.as_view(), name='edit_budget'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/budgets/(?P<budget_pk>[-\w]+)/payments/$', BudgetPaymentListView.as_view(), name='budget_payments'),
    url(r'^provider/add/$', AjaxProviderCreateView.as_view(), name="provider_add"),
    url(r'^buildings/(?P<building_slug>[-\w]+)/budgets/import/$', login_required(BudgetItemImport.as_view()), name='budget_import'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/budgets/process_import$', login_required(BudgetItemProcessImport.as_view()), name='budget_process_import'),
)