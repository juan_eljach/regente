from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from userena.utils import get_user_profile

class DashboardView(TemplateView):
	template_name = "index.html"

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(DashboardView, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(DashboardView, self).get_context_data(**kwargs)
		userprofile = get_user_profile(user=self.request.user)
		context['userprofile'] = userprofile
		return context
	
