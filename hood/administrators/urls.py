from django.conf.urls import include, url
from django.contrib import admin
from .views import DashboardView

urlpatterns = [
    url(r'^dashboard/', DashboardView.as_view(), name='dashboard'),
]