#settings/local.py
import raven
from .base import *
from celery.schedules import crontab
from os.path import dirname
from mixpanel import Mixpanel

TEMPLATE_DEBUG = True

DEBUG = False

ALLOWED_HOSTS = [".regente.co"]

SECRET_KEY = 'e(b*w8-drwcyve8imj@hti7=xogh6fip21csr9t45f_7(^b=e&'

SITE_ID=1

EMAIL_HOST = "smtp.sendgrid.net"
EMAIL_HOST_USER = "juaneljach"
EMAIL_HOST_PASSWORD = "imperionazza100%"
EMAIL_PORT = 587
EMAIL_USE_TLS = True

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "regente_db",
        "USER": "regente",
        "PASSWORD": "B71@e4tW#2wpgB#ZjpctW#2",
        "HOST": "localhost",
        "PORT": "5432",
        }
}

INTERNAL_IPS = ("127.0.0.1",)

GOOGLE_ANALYTICS_PROPERTY_ID = 'UA-73563336-1'

GOOGLE_ANALYTICS_DOMAIN = 'regente.co'

RAVEN_CONFIG = {
    'dsn': 'https://e890af5153c24f9299541085b8e07c04:f651611c535b4dc3ade48a04724c868f@app.getsentry.com/63600',
    # If you are using git, you can also automatically configure the
    # release based on the git info.
    #'release': raven.fetch_git_sha(dirname(dirname(abspath(__file__)))),
}

MIXPANEL = Mixpanel("9e1720b65a3c840adf8d40bea2f1f433")

#CELERYBEAT_SCHEDULE = {
#    'add-price-firstday-month': {
#        'task': 'wallet.tasks.check_feevalue',
#        'schedule': crontab(0, 0, day_of_month='1'),
#    },
#}
