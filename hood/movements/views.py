import datetime
import logging
from django.shortcuts import render, render_to_response, redirect, get_object_or_404
from django.template import defaultfilters
from django.core.urlresolvers import reverse
from django.conf import settings
from django.http import HttpResponseForbidden
from django.views.generic import ListView, DetailView, UpdateView, CreateView, FormView, UpdateView
from django.template import RequestContext
from buildings.models import Building
from budgets.models import Budget, BudgetItem
from meetns.utils import render_to_json_response
from .models import Egress, Income, CashFlow
from .forms import AddEgressForm, AddIncomeForm, CashFlowsCreateForm

try:
	mixpanel = settings.MIXPANEL
except:
	mixpanel = False

logger = logging.getLogger(__name__)

INCOME_CHOICES = (
		('cuotas', 'Cuotas de Administracion'),
		('zonas','Alquiler de Zonas Sociales'),
		('productos','Venta de Productos'),
		('arriendos','Arriendos Comerciales'),
		('seguros','Pagos de Aseguradora'),
)

def format_price(price):
		price = price.replace(",","")
		price = price.replace(".","")
		price = int(price)

		return price

def MovementsTable(request, building_slug):
	building = Building.objects.get(slug__iexact=building_slug)
	try:
		cashflow = CashFlow.objects.get(building=building, cashflow_name="box")
	except:
		try:
			cashflow = CashFlow.objects.get(building=building, cashflow_name="checking")
		except:
			try:
				cashflow = CashFlow.objects.get(building=building, cashflow_name="savings")
			except:
				return redirect('createcashflows', building_slug=building_slug)
	try:
		cashflow_slug = cashflow.slug
	except:
		cashflow.slug = defaultfilters.slugify(cashflow_name)
		cashflow.save()
		cashflow_slug = cashflow.slug
	return redirect('cashflow_type', building_slug=building.slug, cashflow_slug=cashflow_slug)
	#available_amount = cashflow.available_amount
	#egress= Egress.objects.filter(building=building).order_by("-id")
	#incomes = Income.objects.filter(building=building).order_by("-id")
	#budget_items = BudgetItem.objects.all()
	#return render_to_response(
	#	'movements/movements_list.html', 
	#	{'available_amount':available_amount, 'egress':egress, 'incomes':incomes, 'building':building, 'concepts':budget_items, 'incomechoices': INCOME_CHOICES}, 
	#	context_instance=RequestContext(request)
	#)

class CashFlowDetailView(DetailView):
	template_name = "movements/cashflow_detail.html"
	model = CashFlow
	context_object_name = "cashflow"

	def get_object(self, *args, **kwargs):
		building_slug = self.kwargs["building_slug"]
		cashflow_slug = self.kwargs["cashflow_slug"]
		building_object = Building.objects.get(slug__iexact=building_slug)
		obj = get_object_or_404(CashFlow, building=building_object, slug__iexact=cashflow_slug)
		return obj

	def get_context_data(self, *args, **kwargs):
		context = super(CashFlowDetailView, self).get_context_data(*args, **kwargs)
		building_slug = self.kwargs["building_slug"]
		building_object = Building.objects.get(slug__iexact=building_slug)
		cashflow_slug = self.kwargs["cashflow_slug"]
		cashflow_object = CashFlow.objects.get(building=building_object, slug__iexact=cashflow_slug)
		egress= Egress.objects.filter(cashflow__building=building_object, cashflow=cashflow_object).order_by("-id")
		incomes = Income.objects.filter(cashflow__building=building_object, cashflow=cashflow_object).order_by("-id")
		budget = Budget.objects.filter(building=building_object).last()
		budget_items = budget.budgetitem_set.all()
		context["egress"] = egress
		context["incomes"] = incomes
		context["incomechoices"] = INCOME_CHOICES
		context["concepts"] = budget_items
		context["building"] = building_object
		return context

	def get(self, *args, **kwargs):
		building_slug = self.kwargs["building_slug"]
		building_object = Building.objects.get(slug__iexact=building_slug)
		self.object = self.get_object()
		cashflow = self.object
		try:
			available_amount = cashflow.available_amount
		except Exception as lol:
			return self.render_to_response(self.get_context_data())
		if len(available_amount) < 1:
				return redirect('cashflow_update', building_slug=building_object.slug, cashflow_slug=cashflow.slug)
		return self.render_to_response(self.get_context_data())

class CashflowsRegister(FormView):
	template_name = "movements/create_cashflows.html"
	form_class = CashFlowsCreateForm

	def save_cashflow(self, cashflow_name, starting_amount):
		building_slug = self.kwargs['building_slug']
		building_object = Building.objects.get(slug__iexact=building_slug)
		try:
			#This format_price function can raise an exception cuz is trying 2 convert a char field to integer
			starting_amount = format_price(starting_amount)
			if starting_amount > 0:
				CashFlow.objects.create(
					building=building_object,
					cashflow_name=cashflow_name,
					starting_amount=starting_amount,
					available_amount=starting_amount,
				)
				return True
		except Exception as lol:
			if len(starting_amount) < 1:
				CashFlow.objects.create(
					building=building_object,
					cashflow_name=cashflow_name,
					starting_amount="",
					available_amount=""
				)
				return True
	def get_success_url(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		return reverse('create_wallet', kwargs={'building_slug': building_slug})

	def get(self, *args, **kwargs):
		form = self.form_class
		return self.render_to_response(self.get_context_data(form=form))

	def form_valid(self, form):
		building_slug = self.kwargs['building_slug']
		building_object = Building.objects.get(slug__iexact=building_slug)
		has_saving_account = form.cleaned_data["has_saving_account"]
		saving_account_starting_amount = form.cleaned_data["saving_account_starting_amount"]
		has_checking_account = form.cleaned_data["has_checking_account"]
		checking_account_starting_amount = form.cleaned_data["checking_account_starting_amount"]
		has_cashflow_box = form.cleaned_data["has_cashflow_box"]
		cashflow_box_starting_amount = form.cleaned_data["cashflow_box_starting_amount"]

		#Si tiene una cuenta, intentar formatear el saldo de esa cuenta y verificar si es mayor a 1
		#en caso de que sea menor a 1 (el len) levantar excepcion o levantar excepcion si el saldo es cero
		#levantar excepcion para registrar el cashflow con valores en blanco para los saldos disponibles e iniciales


		if has_saving_account == True:
			self.save_cashflow(cashflow_name="savings", starting_amount=saving_account_starting_amount)
		if has_checking_account == True:
			self.save_cashflow(cashflow_name="checking", starting_amount=checking_account_starting_amount)
		if has_cashflow_box == True:
			self.save_cashflow(cashflow_name="box", starting_amount=cashflow_box_starting_amount)

		try:
			if mixpanel:
				mixpanel.track(self.request.user.id, 'Registro de cajas completado')
		except Exception as error:
			logger.error("No se pudo trackear el registro de cajas en Mixpanel: {0}".format(error), exc_info=True, extra={
                    'request': self.request,
            })

		success_url = self.get_success_url()
		return redirect(success_url)

class CashFlowUpdateView(UpdateView):
	template_name = "movements/update_cashflow.html"
	model = CashFlow
	fields = ["starting_amount"]

	def get_object(self, *args, **kwargs):
		building_slug = self.kwargs["building_slug"]
		cashflow_slug = self.kwargs["cashflow_slug"]
		building_object = Building.objects.get(slug__iexact=building_slug)
		obj = CashFlow.objects.get(building=building_object, slug__iexact=cashflow_slug)
		return obj

	def get_success_url(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		cashflow = self.get_object()
		return reverse('cashflow_type', kwargs={'building_slug': building_slug, 'cashflow_slug':cashflow.slug})

	def get_context_data(self, *args, **kwargs):
		context = super(CashFlowUpdateView, self).get_context_data(*args, **kwargs)
		building_slug = self.kwargs["building_slug"]
		building_object = Building.objects.get(slug__iexact=building_slug)
		context["building"] = building_object
		return context

	def form_valid(self, form):
		starting_amount = form.cleaned_data["starting_amount"]
		form.instance.available_amount = starting_amount
		return super(CashFlowUpdateView, self).form_valid(form)

#class CashFlowCreateView(CreateView):
#	template_name = "movements/create_cashflow.html"
#	model = CashFlow
#	fields = ["starting_amount"]

#	def get_success_url(self, *args, **kwargs):
#		building_slug = self.kwargs['building_slug']
#		return reverse('cashflow', kwargs={'building_slug': building_slug})

#	def get_context_data(self, **kwargs):
#		context = super(CashFlowCreateView, self).get_context_data(**kwargs)
#		building_slug = self.kwargs['building_slug']
#		building_object = Building.objects.get(slug__iexact=building_slug)
#		context['building'] = building_object
#		return context

#	def post(self, *args, **kwargs):
#		building_slug = self.kwargs['building_slug']
#		building_object = Building.objects.get(slug__iexact=building_slug)
#		form = self.get_form()
#		if form.is_valid():
#			cashflow = form.save(commit=False)
#			cashflow.building = building_object
#			cashflow.available_amount = cashflow.starting_amount
#			cashflow.save()

#			success_url = self.get_success_url()
#			return redirect(success_url)
#		else:
#			print (form.errors)
#			return self.render_to_response(self.get_context_data(form=form))

class AjaxFormEgressResponseMixin(object):
	def format_price(self, price):
		price = price.replace(",","")
		price = price.replace(".","")
		price = int(price)

		return price

	def form_invalid(self, form):
		return render_to_json_response(form.errors, status=400)

	def form_valid(self, form):
		print ("YEAH BABY, IM IN, IT'S WORKED")
		self.object = form.save()
		cashflow = CashFlow.objects.get(building=self.object.building)
		price = self.format_price(self.object.price)
		available_amount = self.format_price(cashflow.available_amount)
		available_amount = available_amount - price
		cashflow.available_amount = available_amount
		cashflow.save()
		context = {}
		return render_to_json_response(self.get_context_data(context))

def create_egress(request, building_slug, cashflow_slug):
	if request.method == "POST":
		try:
			date = request.POST.get("date")
			date_format = datetime.datetime.strptime(date, "%m/%d/%Y")
			valid_date = datetime.date(year=date_format.year, month=date_format.month, day=date_format.day)
			request.POST = request.POST.copy()
			request.POST['date'] = valid_date
		except Exception as lol:
			print (lol)

		try:
			form = AddEgressForm(request.POST)
		except Exception as lol:
			print (lol)

		if form.is_valid():
			egress_object = form.save()
		else:
			return render_to_json_response(form.errors, status=400)

		print ("GOT THE OBJECT")
		print (egress_object)
		valid_price = format_price(egress_object.price)
		cashflow = CashFlow.objects.get(building__slug__iexact=building_slug, slug__iexact=cashflow_slug)
		available_amount = format_price(cashflow.available_amount)
		available_amount = available_amount - valid_price
		cashflow.available_amount = available_amount
		cashflow.save()

		desired_format = "%d %B %Y"
		date = egress_object.date
		date = date.strftime(desired_format)

		context = {}
		context["egress_id"] = egress_object.id
		context["available_amount"] = cashflow.available_amount
		concept = egress_object.concept.concept
		context["concept"] = concept
		context["price"] = egress_object.price
		context["date"] = date

		try:
			return render_to_json_response(context)
		except Exception as lol:
			print (lol)
	else:
		return HttpResponseForbidden()



def create_income(request, building_slug, cashflow_slug):
	if request.method == "POST":
		date = request.POST.get("date")
		date_format = datetime.datetime.strptime(date, "%m/%d/%Y")
		valid_date = datetime.date(year=date_format.year, month=date_format.month, day=date_format.day)
		request.POST = request.POST.copy()
		request.POST['date'] = valid_date

		form = AddIncomeForm(request.POST)

		if form.is_valid():
			income_object = form.save()
		else:
			print (form.errors)
			return render_to_json_response(form.errors, status=400)

		valid_price = format_price(income_object.price)
		cashflow = CashFlow.objects.get(building__slug__iexact=building_slug, slug__iexact=cashflow_slug)
		available_amount = format_price(cashflow.available_amount)
		available_amount = available_amount + valid_price
		cashflow.available_amount = available_amount
		cashflow.save()

		desired_format = "%d %B %Y"
		date = income_object.date
		date = date.strftime(desired_format)

		context = {}
		context["income_id"] = income_object.id
		context["available_amount"] = cashflow.available_amount
		concept = income_object.get_concept_display()
		context["concept"] = concept
		context["price"] = income_object.price
		context["date"] = date

		try:
			return render_to_json_response(context)
		except Exception as lol:
			print (lol)
	else:
		return HttpResponseForbidden()


#	def get_context_data(self, context):
#		cashflow_slug = self.kwargs["cashflow_slug"]
#		cashflow = CashFlow.objects.get(building=self.object.building, slug__iexact=cashflow_slug)
#		context['available_amount'] = cashflow.available_amount
#		context['concept'] = self.object.get_concept_display()
#		context['price'] = self.object.price
#		try:
#			context['income_id'] = self.object.id
#		except Exception as lol:
#			print (lol)
#		desired_format = "%d %B %Y"
#		date = self.object.date
#		print (date)
#		print (type(date))
#		date = date.strftime(desired_format)
#		context['date'] = date
#		return context
		

#VIEW TO USE FROM WALLET MODULE IN ORDER TO CREATE AN INCOME WHEN A PAYMENT IS DONE
def add_income(cashflow, price, concept, date):
	try:
		print ("THIS IS THE CASHFLOW")
		print (cashflow)
		income_object = Income.objects.create(cashflow=cashflow, price=price, concept=concept, date=date)
		print ("GONNA PRINT CASHFLOW AND ITS TYPE")
		available_amount = format_price(cashflow.available_amount)
		available_amount = available_amount + price
		print (available_amount)
		cashflow.available_amount = available_amount
		cashflow.save()
		return True
	except Exception as lol:
		print (lol)
		return False


def delete_income(request, income_id, cashflow_slug):
	print ("GOT INTO FUNC")
	income = get_object_or_404(Income, id=income_id)
	print ("GOT INCOME")
	print (income)
	try:
		income_price = format_price(income.price)
	except Exception as inst:
		print (inst)
	try:	
		cashflow = income.cashflow
		available_amount = format_price(cashflow.available_amount)
		cashflow.available_amount = available_amount - income_price
		cashflow.save()
	except Exception as lol:
		print (lol)
	try:
		income.delete()
	except Exception as inst:
		print (inst)
	context = {"deleted": True, 'newavailable_amount':cashflow.available_amount}
	return render_to_json_response(context)

def delete_egress(request, egress_id, cashflow_slug):
	egress = get_object_or_404(Egress, id=egress_id)
	egress_building = egress.building
	try:
		egress_price = format_price(egress.price)
	except Exception as inst:
		print (inst)
	try:
		cashflow = CashFlow.objects.get(building=egress_building, slug__iexact=cashflow_slug)
		available_amount = format_price(cashflow.available_amount)
		cashflow.available_amount = available_amount + egress_price
		cashflow.save()
	except Exception as lol:
		print (lol)
	try:
		egress.delete()
	except Exception as lol:
		print (lol)
	context = {"deleted": True, 'newavailable_amount':cashflow.available_amount}
	return render_to_json_response(context)