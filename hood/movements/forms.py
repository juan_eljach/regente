from django import forms
from django.forms import ModelForm
from .models import Egress, Income, CashFlow

class CashFlowsCreateForm(forms.Form):
	has_saving_account = forms.TypedChoiceField(coerce=lambda x: x == 'True', choices=((True, 'Si'), (False, 'No')), widget=forms.RadioSelect, required=False)
	has_checking_account = forms.TypedChoiceField(coerce=lambda x: x == 'True', choices=((True, 'Si'), (False, 'No')), widget=forms.RadioSelect, required=False)
	has_cashflow_box = forms.TypedChoiceField(coerce=lambda x: x == 'True', choices=((True, 'Si'), (False, 'No')), widget=forms.RadioSelect, required=False)
	saving_account_starting_amount = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Saldo Disponible (Opcional)'}), label='', required=False)
	checking_account_starting_amount = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Saldo Disponible (Opcional)'}), label='', required=False)
	cashflow_box_starting_amount = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Saldo Disponible (Opcional)'}), label='', required=False)


#class CashFlowsCreateForm(ModelForm):
#	has_saving_account = forms.TypedChoiceField(coerce=lambda x: x == 'True', choices=((True, 'Si'), (False, 'No')), widget=forms.RadioSelect)
#	has_checking_account = forms.TypedChoiceField(coerce=lambda x: x == 'True', choices=((True, 'Si'), (False, 'No')), widget=forms.RadioSelect)
#	has_cashflow_box = forms.TypedChoiceField(coerce=lambda x: x == 'True', choices=((True, 'Si'), (False, 'No')), widget=forms.RadioSelect)
	
#	class Meta:
#		model = CashFlow
#		fields = ["has_saving_account", "has_checking_account", "has_cashflow_box", "saving_account_starting_amount", "checking_account_starting_amount", "cashflow_box_starting_amount"]

class AddEgressForm(ModelForm):
    class Meta:
        model = Egress
        fields = ["cashflow", "price", "concept", "comment", "date"]

class AddIncomeForm(ModelForm):
    class Meta:
        model = Income
        fields = ["cashflow", "price", "concept", "comment", "date"]