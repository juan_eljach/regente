from django.contrib import admin
from .models import Egress, Income, CashFlow

admin.site.register(Egress)
admin.site.register(Income)
admin.site.register(CashFlow)
# Register your models here.
