from django.conf.urls import patterns, url
from .views import MovementsTable, CashFlowDetailView, CashFlowUpdateView, CashflowsRegister, create_egress, create_income, delete_income, delete_egress

urlpatterns = patterns('',
	url(r'^buildings/(?P<building_slug>[-\w]+)/cashflows/create$', CashflowsRegister.as_view(), name='createcashflows'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/cashflow/$', MovementsTable, name='cashflow'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/cashflow/(?P<cashflow_slug>[-\w]+)$', CashFlowDetailView.as_view(), name='cashflow_type'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/cashflow/(?P<cashflow_slug>[-\w]+)/update$', CashFlowUpdateView.as_view(), name='cashflow_update'),
    url(r'^egress/(?P<building_slug>[-\w]+)/add/(?P<cashflow_slug>[-\w]+)$', create_egress, name='addegress'),
    url(r'^income/(?P<building_slug>[-\w]+)/add/(?P<cashflow_slug>[-\w]+)$', create_income, name='addincome'),
    url(r'^income/(?P<income_id>[0-9]+)/delete/(?P<cashflow_slug>[-\w]+)$', delete_income, name="income_delete"),
    url(r'^egress/(?P<egress_id>[0-9]+)/delete/(?P<cashflow_slug>[-\w]+)$', delete_egress, name="egress_delete"),
)