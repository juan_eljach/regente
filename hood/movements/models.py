from django.db import models
from django.template import defaultfilters
from budgets.models import BudgetItem
from buildings.models import Building

INCOME_CHOICES = (
		('cuotas', 'Cuotas de Administracion'),
		('zonas','Alquiler de Zonas Sociales'),
		('productos','Venta de Productos'),
		('arriendos','Arriendos Comerciales'),
		('seguros','Pagos de Aseguradora'),
)


CASHFLOW_CHOICES = (
	("savings", "Caja de Cuenta de Ahorros"),
	("checking", "Caja de Cuenta Corriente"),
	("box", "Caja Menor")
)

class CashFlow(models.Model):
	building = models.ForeignKey(Building)
	cashflow_name = models.CharField(choices=CASHFLOW_CHOICES, max_length=100, blank=True)
	starting_amount = models.CharField(max_length=50, blank=True)
	available_amount = models.CharField(max_length=50, blank=True)
	slug = models.SlugField(max_length=500)

	def save(self, *args, **kwargs):
		self.slug = defaultfilters.slugify(self.cashflow_name)
		super(CashFlow, self).save(*args, **kwargs)

	def get_siblings_cashflows(self):
		sinbling_cashflows = CashFlow.objects.filter(building=self.building).exclude(id=self.id)
		return sinbling_cashflows

	def __str__(self):
		return '{0}'.format(self.get_cashflow_name_display())

class Egress(models.Model):
	cashflow = models.ForeignKey(CashFlow)
	price = models.CharField(max_length=50)
	concept = models.ForeignKey(BudgetItem, null=True, on_delete=models.SET_NULL)
	comment = models.TextField(blank=True)
	date = models.DateField()

	def __str__(self):
		return self.concept.concept

class Income(models.Model):
	cashflow = models.ForeignKey(CashFlow)
	price = models.CharField(max_length=50)
	concept = models.CharField(choices=INCOME_CHOICES, max_length=50)
	comment = models.TextField(blank=True)
	date = models.DateField(blank=True, null=True)

	def __str__(self):
		return self.concept
