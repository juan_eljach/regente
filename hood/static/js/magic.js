function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

function AddMessage(event){
   var csrftoken = getCookie('csrftoken');
   var name = $('#name').val();
   var description = $('#description').val();
   var priority = $('#priority').val();
   var meeting = $('#meeting').val();
   var url = "/item/add/";
   $.ajax({
       type: "POST",
       url: url,
       data: {
           'name': name,
           'description': description,
           'priority': priority,
           'meeting': meeting,
           'csrfmiddlewaretoken': csrftoken
       },
       success: function(data){
        $('#name').val("");
        $('#description').val("");
        var name = data['name'];
        var description = data['description'];
        var done = data['done'];
        $('.item-list').append("<li><a class='aIcon' id='check' href='#' onclick='ItemDone()'><i class='icon un-check'></i></a><a class='aIcon' id='delete' href='#'><i class='icon  fa-trash-o'></i></a><p>"+name+": "+description+" - "+done+"</p></li>");

       }
   });
}

function AddProvider(event){
   var csrftoken = getCookie('csrftoken');
   var name = $('#providername').val();
   var phone = $('#providerphone').val();
   var email = $('#provideremail').val();
   var address = $('#provideraddress').val();
   var service = $('#providerservice').val();
   var bankaccount = $('#providerbankaccount').val();
   var url = "/provider/add/";
   $.ajax({
       type: "POST",
       url: url,
       data: {
           'name': name,
           'phone_number': phone,
           'email': email,
           'address': address,
           'service': service,
           'bank_account_number': bankaccount,
           'csrfmiddlewaretoken': csrftoken
       },
       success: function(data){
        var id = data['id'];
        var name = data['name'];
        $('.providers').append($('<option>', {
            'value': id,
            'text': name
        }));
        $('.formFlow').val('');
       }
   });
   
}

function ItemDone(id){
   var id = id;
   var url = "/meetings/"+id+"/done/";
   $.ajax({
       url: url,
       type: "GET",
       success: function(data){
        var done = data["done"];
       }
   });
}

function ItemDelete(id){
   var id = id;
   var url = "/meetings/"+id+"/delete/";
   $.ajax({
       url: url,
       type: "GET",
       success: function(data){
        alert("Item eliminado");
       }
   });
}

function PaymentPaid(element){
   var id = $(element).attr("id");
   var url = "/wallet/"+id+"/paid/";
   $.ajax({
       url: url,
       type: "GET",
       success: function(data){
        var new_payment_record_date = data['payment_record_date'];
        var error = data["error"]
        var closest_td = $(element).closest("td");
        closest_td.siblings("#payment_record_date").fadeOut("normal", function() {
          $(this).replaceWith("<td id='payment_record_date'>"+ new_payment_record_date +"</td>");
        });
        closest_td.fadeOut("normal", function() {
          $(this).replaceWith("<td class='aIcon'><i class='icon fa-check'></i></td>");
        });
        if (error == true){
          $("#dialog2").dialog("open");
        }
        else{
          $("#dialog1").dialog("open");
        }
       }
   });
}

/*function InlinePaymentPaid(element){
   var id = $(element).attr("id");
   var url = "/inlinewallet/"+id+"/paid/";
   $.ajax({
       url: url,
       type: "GET",
       success: function(data){
        var new_next_payment_date = data["new_next_payment_date"]
        var new_next_payment_fee = data["new_next_payment_value"]
        var error = data["error"]
        var next_payment_error = data["next_payment_error"]
        var closest_td = $(element).closest("td");
        if (next_payment_error == true){
          console.log("THERE WAS AN ERROR, SO YA KNOW")
          closest_td.siblings("#next_payment_date").fadeOut("slow", function() {
            $(this).replaceWith("<td id='next_payment_date'>No hay pago siguiente registrado</td>");
          });
          closest_td.siblings("#next_payment_fee_value").fadeOut("slow", function() {
            $(this).replaceWith("<td id='next_payment_fee_value'>No hay valor registrado para el siguiente pago</td>");
          });
        }
        else{
          console.log("GONNA START DOING SHIT")
          closest_td.siblings("#next_payment_date").fadeOut("slow", function() {
            $(this).replaceWith("<td id='next_payment_date'>"+ new_next_payment_date +"</td>");
          });
          console.log("DID FIRST SHIT")
          closest_td.siblings("#next_payment_fee_value").fadeOut("slow", function() {
            $(this).replaceWith("<td id='next_payment_fee_value'>$"+ new_next_payment_fee +"</td>");
          });
          console.log("CONSOLE DIG 2ND SHIT")
          if (error == true){
            $("#dialog2").dialog("open");
          }
        }
      }
   });
}*/

function InlinePaymentPaid(element){
   //alert("Entro a la funcion");
   $('.apartment_payment_box').removeClass('yes not');

   var id = $('#payment_id').val();
   var csrftoken = getCookie('csrftoken');
   var payment_record_date = $('#payment_record_date').val();
   var fee_value = $('#fee_value').val();
   var discount = $('.switchBtn-input:checked').val();
   var building_id = $('#building_id').val();
   var lol = "/walletbymonth/"+id+"/paid/";

   $.ajax({
       type: "POST",
       url: lol,
       data: {
           'payment_record_date': payment_record_date,
           'fee_value': fee_value,
           'has_discount': discount,
           'building_id': building_id,
           'csrfmiddlewaretoken': csrftoken
       },
       success: function(data){
        
        var new_next_payment_id = data["new_next_payment_id"]
        var new_next_payment_apartment = data["new_next_payment_apartment"]
        var new_next_payment_owner = data["new_next_payment_owner"]
        var new_next_payment_value = data["new_next_payment_value"]
        var new_next_payment_apartment_next = data["new_next_payment_apartment_next"]
        var discount_early_payment_amount = data["discount_early_payment_amount"]

        $('.apartment_payment_box').addClass('yes');

        if(new_next_payment_owner == null){new_next_payment_owner = 'No hay propietario registrado'}
        $('#apartment_name').text(new_next_payment_apartment)
        $('#apartment_owner').text(new_next_payment_owner)
        $('#payment_id').val(new_next_payment_id)
        $('#fee_value').val(new_next_payment_value)
        $('#payment_record_date').val('')
        $('#discount_value').text(discount_early_payment_amount)
        $('#next_apartment_next').text(new_next_payment_apartment_next)
        
      },
      error: function(data){
        alert(data);
      }
   });
}

function InlinePaymentNoPay(element){
   //alert("Entro a la funcion");
   $('.apartment_payment_box').removeClass('yes not');

   var id = $('#payment_id').val();
   var csrftoken = getCookie('csrftoken');
   var lol = "/walletbymonth/"+id+"/next/";

   $.ajax({
       type: "GET",
       url: lol,
        success: function(data){
        
        var new_next_payment_id = data["new_next_payment_id"]
        var new_next_payment_apartment = data["new_next_payment_apartment"]
        var new_next_payment_owner = data["new_next_payment_owner"]
        var new_next_payment_value = data["new_next_payment_value"]
        var new_next_payment_apartment_next = data["new_next_payment_apartment_next"]

        $('.apartment_payment_box').addClass('not');

        if(new_next_payment_owner == null){new_next_payment_owner = 'No hay propietario registrado'}
        $('#apartment_name').text(new_next_payment_apartment)
        $('#apartment_owner').text(new_next_payment_owner)
        $('#payment_id').val(new_next_payment_id)
        $('#fee_value').val(new_next_payment_value)
        $('#payment_record_date').val('')
        $('#next_apartment_next').text(new_next_payment_apartment_next)
      
      },
      error: function(data){
        alert(data);
      }
   });
}

function SearchPayment(payment_id){
   //alert("Entro a la funcion");
   var id = payment_id;
   var csrftoken = getCookie('csrftoken');
   var lol = "/walletbymonth/"+id+"/";

   $.ajax({
       type: "GET",
       url: lol,
        success: function(data){
        
        var new_next_payment_id = data["new_next_payment_id"]
        var new_next_payment_apartment = data["new_next_payment_apartment"]
        var new_next_payment_owner = data["new_next_payment_owner"]
        var new_next_payment_value = data["new_next_payment_value"]
        var new_next_payment_apartment_next = data["new_next_payment_apartment_next"]

        $('.apartment_payment_box').addClass('not');

        if(new_next_payment_owner == null){new_next_payment_owner = 'No hay propietario registrado'}
        $('#apartment_name').text(new_next_payment_apartment)
        $('#apartment_owner').text(new_next_payment_owner)
        $('#payment_id').val(new_next_payment_id)
        $('#fee_value').val(new_next_payment_value)
        $('#payment_record_date').val('')
        $('#next_apartment_next').text(new_next_payment_apartment_next)
      
      },
      error: function(data){
        alert(data);
      }
   });
}

function AddEgress(event){
   var csrftoken = getCookie('csrftoken');
   var cashflow_slug = $('#cashflow_slug').val();
   var building_slug = $('#building_slug').val();
   var cashflow = $('#cashflow').val();
   var building = $('#building').val();
   var price = $('#egressprice').val();
   var concept = $('#egressconcept').val();
   var comment = $('#egresscomment').val();
   var date = $('#egress_date').val();
   var url = "/egress/"+ building_slug +"/add/"+cashflow_slug;
   console.log(concept);
   $.ajax({
       type: "POST",
       url: url,
       data: {
           'cashflow': cashflow,
           'building': building,
           'price': price,
           'concept': concept,
           'comment': comment,
           'date': date,
           'csrfmiddlewaretoken': csrftoken
       },
       success: function(data){
        var newavailable_amount = data['available_amount']
        var newconcept = data['concept'];
        var newprice = data['price'];
        var newdate = data['date'];
        var Egress_ID = data['egress_id']
        alert("think it has an id")
        alert(Egress_ID);
        $('#bodytable-egress').prepend("<tr><td>"+newconcept+"</td><td><strong>$"+newprice+"</strong></td><td>"+newdate+"</td><td class='actions'><a id='"+Egress_ID+"' href='#' class='on-default remove-row' onclick='RemoveEgress(this)'><i style='color: #ef5350;'class='fa fa-trash-o'></i></a></td></tr>")
        $('.formFlow').val('');
        $('#myModal').modal('hide');
        $('#amount').html("<h1 style='font-size: 48px; margin-bottom: 0.5em;' id='amount'>$"+newavailable_amount+"<span style='font-size: 16px'>(En caja)</span></h1>")

       }
   });
}

function AddIncome(event){
   var csrftoken = getCookie('csrftoken');
   var cashflow_slug = $('#cashflow_slug').val();
   var building_slug = $('#building_slug').val();
   var cashflow = $('#cashflow').val();
   var building = $('#building').val();
   var price = $('#incomeprice').val();
   var concept = $('#incomeconcept').val();
   var comment = $('#incomecomment').val();
   var date = $('#income_date').val();
   var url = "/income/"+ building_slug +"/add/"+cashflow_slug;
   $.ajax({
       type: "POST",
       url: url,
       data: {
           'cashflow': cashflow,
           'building': building,
           'price': price,
           'concept': concept,
           'comment': comment,
           'date': date,
           'csrfmiddlewaretoken': csrftoken
       },
       success: function(data){
        var newavailable_amount = data['available_amount']
        var newconcept = data['concept'];
        var newprice = data['price'];
        var newdate = data['date'];
        var Income_ID = data['income_id']
        $('#bodytable-incomes').prepend("<tr><td>"+newconcept+"</td><td><strong>$"+newprice+"</strong></td><td>"+newdate+"</td><td class='actions'><a id='"+Income_ID+"' href='#' class='on-default remove-row' onclick='RemoveIncome(this)'><i style='color: #ef5350;'class='fa fa-trash-o'></i></a></td></tr>")
        $('.formFlow').val('');
        $('#myModal2').modal('hide');
        $('#amount').html("<h1 style='font-size: 48px; margin-bottom: 0.5em;' id='amount'>$"+newavailable_amount+"<span style='font-size: 16px'>(En caja)</span></h1>")
       },
       error: function(data){
        alert("There was an error");
        alert(data);
       }
   });
   
}

function RemoveIncome(element){
        var IncomeID = $(element).attr("id");
        var cashflow_slug = $('#cashflow_slug').val();
        var url = "/income/"+IncomeID+"/delete/"+cashflow_slug;
        $("#dialog1").dialog('option', 'buttons', {
            "Sí, seguro(a)" : function() {
                $.ajax({
                    url: url,
                    type: "GET",
                    success: function(data){
                        $("#dialog1").dialog("close");
                        $(element).closest("tr").fadeOut("normal", function() {
                          $(this).remove()
                        });
                        if (data['newavailable_amount'] !=''){
                          newavailable_amount = data['newavailable_amount'];
                          $('#amount').html("<h1 style='font-size: 48px; margin-bottom: 0.5em;' id='amount'>$"+newavailable_amount+"<span style='font-size: 16px'>(En caja)</span></h1>")
                        }
                    },
                    error: function(data){
                      alert("There was an error");
                    }
                });
            },
            "Cancelar" : function() {
                $(this).dialog("close");
            }
        });
        $("#dialog1").dialog("open");
    }

function RemoveEgress(element){
        var EgressID = $(element).attr("id");
        var cashflow_slug = $('#cashflow_slug').val();
        var url = "/egress/"+EgressID+"/delete/"+cashflow_slug;
        $("#dialog2").dialog('option', 'buttons', {
            "Confirmar" : function() {
                $.ajax({
                    url: url,
                    type: "GET",
                    success: function(data){
                        $("#dialog2").dialog("close");
                        $(element).closest("tr").fadeOut("normal", function() {
                          $(this).remove()
                        });
                        if (data['newavailable_amount'] !=''){
                          newavailable_amount = data['newavailable_amount'];
                          $('#amount').html("<h1 style='font-size: 48px; margin-bottom: 0.5em;' id='amount'>$"+newavailable_amount+"<span style='font-size: 16px'>(En caja)</span></h1>")
                        }
                    }
                });
            },
            "Cancelar" : function() {
                $(this).dialog("close");
            }
        });
        $("#dialog2").dialog("open");
    }

function CurrencyFormat(){
  $('.mPrice').each(function(){
    var mprice = $(this).text().replace('$','').replace('.','').replace(',','');
    //alert(mprice);
    var m_value = numeral(mprice).format('0,0');  
    var m_value = m_value.replace(/\,/g,".");

    $(this).text('$'+m_value);
  });
}

function SaveProvider(event){
  var providername = $('#providername').val();
  var providerphone = $('#providerphone').val();
  if(providername != '')
  {
    $.ajax({
        url: '/newprovider/',
        type: "POST",
        data: { name: providername, phone: providerphone },
        success: function(data){
            var id = data["providerid"];
            $('#egressprovider').append('<option value="'+id+'" selected>'+providername+'</option>');
            $('.closeProvBtn').html('<i class="fa fa-plus-circle"></i> Agregar Proveedor').removeClass('closeProvBtn').addClass('addProvBtn');
            $('#provider').toggle();
            $('#providername').val('');
            $('#providerphone').val('');
            $('.spinner').css('display','block');
        }
    });
  }
  else
  {
    alert('Por favor ingrese un nombre de proveedor.');
  }
}