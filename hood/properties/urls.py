from django.conf.urls import patterns, url
from .views import ApartmentsListView, ApartmentDetailView, ApartmentUpdateView, OwnerUpdateView, InhabitantUpdateView, VisitCreateView, ApartmentImport, ApartmentProcessImport, ApartmentExport, OwnerImport, OwnerExport, OwnerProcessImport, InhabitantImport, InhabitantExport, InhabitantProcessImport
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('',
    url(r'^buildings/(?P<building_slug>[-\w]+)/properties/$', ApartmentsListView.as_view(), name='properties'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/properties/import$', login_required(ApartmentImport.as_view()), name='properties_import'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/properties/(?P<apartment_pk>[-\w]+)$', ApartmentDetailView.as_view(), name='property_detail'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/properties/(?P<apartment_pk>[-\w]+)/edit/$', ApartmentUpdateView.as_view(), name='property_edit'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/properties/(?P<apartment_pk>[-\w]+)/edit/owner/$', OwnerUpdateView.as_view(), name='property_owner_edit'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/properties/(?P<apartment_pk>[-\w]+)/edit/inhabitant/(?P<inhabitant_pk>[0-9a-f])/$', InhabitantUpdateView.as_view(), name='property_inhabitant_edit'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/properties/(?P<apartment_pk>[-\w]+)/visit/create$', VisitCreateView.as_view(), name='visit_create'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/properties/export/$', ApartmentExport.as_view(), name='properties_export'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/properties/import/$', login_required(ApartmentImport.as_view()), name='data_import'),
    url(r'^(?P<building_slug>[-\w]+)/process_import$', login_required(ApartmentProcessImport.as_view()), name='process_import'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/owners/import/$', login_required(OwnerImport.as_view()), name='owner_import'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/owners/export/$', login_required(OwnerExport.as_view()), name='owner_export'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/inhabitants/export/$', login_required(InhabitantExport.as_view()), name='inhabitant_export'),
    url(r'^(?P<building_slug>[-\w]+)/owners/process_import$', login_required(OwnerProcessImport.as_view()), name='owner_process_import'),
    url(r'^buildings/(?P<building_slug>[-\w]+)/inhabitants/import/$', login_required(InhabitantImport.as_view()), name='inhabitant_import'),
    url(r'^(?P<building_slug>[-\w]+)/inhabitants/process_import$', login_required(InhabitantProcessImport.as_view()), name='inhabitant_process_import'),
)