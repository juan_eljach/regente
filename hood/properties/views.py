import csv
import logging
import os
import tempfile
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView, DetailView, UpdateView, CreateView, View
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator
from django.utils.encoding import force_text
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.conf import settings
from django.template.response import TemplateResponse
from django.http import HttpResponse
from import_export.forms import ImportForm
from import_export.forms import ConfirmImportForm
from import_export.formats import base_formats
from import_export.resources import modelresource_factory
from import_export import resources
from userena.utils import get_user_profile
from buildings.models import Building
from .decorators import verify_data
from .models import Apartment, Owner, Inhabitant, Visit, ApartmentResource
from .forms import ApartmentUpdateForm

try:
    mixpanel = settings.MIXPANEL
except:
    mixpanel = False

logger = logging.getLogger(__name__)

class ApartmentsListView(ListView):
	template_name = "properties/apartments.html"
	model = Apartment
	context_object_name = "apartments"

	@method_decorator(verify_data)
	def dispatch(self, *args, **kwargs):
		return super(ApartmentsListView, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(ApartmentsListView, self).get_context_data(**kwargs)
		userprofile = get_user_profile(user=self.request.user)
		building_slug = self.kwargs['building_slug']
		building_object = Building.objects.get(slug__iexact=building_slug)
		apartments_in_building = self.model.objects.filter(building=building_object)
		context['building'] = building_object
		context['apartments'] = apartments_in_building
		context['userprofile'] = userprofile
		return context

class ApartmentDetailView(DetailView):
	template_name = "properties/apartment_detail.html"
	model = Apartment
	context_object_name = "apartment"

	def get_object(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		apartment_pk = self.kwargs['apartment_pk']
		building_object = Building.objects.get(slug__iexact=building_slug)
		obj = self.model.objects.get(pk=apartment_pk, building=building_object)
		return obj

	def get_context_data(self, *args, **kwargs):
		context = super(ApartmentDetailView, self).get_context_data(*args, **kwargs)
		userprofile = get_user_profile(user=self.request.user)
		building_slug = self.kwargs['building_slug']
		apartment_pk = self.kwargs['apartment_pk']
		building_object = Building.objects.get(slug__iexact=building_slug)
		apartment_object = self.model.objects.get(pk=apartment_pk, building=building_object)
		context['building'] = building_object
		context['inhabitants'] = apartment_object.inhabitant_set.all()
		context['userprofile'] = userprofile
		try:
			owner = apartment_object.owner_set.all()[0]
		except:
			owner = None
		context['owner'] = owner
		context['visits'] = apartment_object.visit_set.all()
		return context

class ApartmentUpdateView(UpdateView):
	template_name = "properties/edit_apartment.html"
	model = Apartment
	fields = ['name', 'registration_tag', 'area', 'parking_number']

	@method_decorator(permission_required("properties.change_apartment", raise_exception=True))
	def dispatch(self, *args, **kwargs):
		return super(ApartmentUpdateView, self).dispatch(*args, **kwargs)

	def get_object(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		apartment_pk = self.kwargs['apartment_pk']
		building_object = Building.objects.get(slug__iexact=building_slug)
		obj = self.model.objects.get(pk=apartment_pk, building=building_object)
		return obj

	def get_success_url(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		apartment_pk = self.kwargs['apartment_pk']
		return reverse('property_detail', kwargs={'building_slug': building_slug, 'apartment_pk': apartment_pk})

class OwnerUpdateView(UpdateView):
	template_name = "properties/edit_apartment_owner.html"
	model = Owner
	fields = ['name', 'last_name', 'email', 'phone_number', 'address']

	@method_decorator(permission_required("properties.change_owner", raise_exception=True))
	def dispatch(self, *args, **kwargs):
		return super(OwnerUpdateView, self).dispatch(*args, **kwargs)

	def get_object(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		apartment_pk = self.kwargs['apartment_pk']
		building_object = Building.objects.get(slug__iexact=building_slug)
		apartment_object = Apartment.objects.get(pk=apartment_pk, building=building_object)
		obj = apartment_object.owner_set.all()[0]
		return obj

	def get_success_url(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		apartment_pk = self.kwargs['apartment_pk']
		return reverse('property_detail', kwargs={'building_slug': building_slug, 'apartment_pk': apartment_pk})

class InhabitantUpdateView(UpdateView):
	template_name = "properties/edit_apartment_inhabitant.html"
	model = Inhabitant
	fields = ['name', 'last_name', 'email', 'phone_number', 'address']

	@method_decorator(permission_required("properties.change_inhabitant", raise_exception=True))
	def dispatch(self, *args, **kwargs):
		return super(InhabitantUpdateView, self).dispatch(*args, **kwargs)

	def get_object(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		apartment_pk = self.kwargs['apartment_pk']
		inhabitant_pk = self.kwargs['inhabitant_pk']
		building_object = Building.objects.get(slug__iexact=building_slug)
		apartment_object = Apartment.objects.get(pk=apartment_pk, building=building_object)
		obj = apartment_object.inhabitant_set.get(pk=inhabitant_pk)
		return obj

	def get_success_url(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		apartment_pk = self.kwargs['apartment_pk']
		return reverse('property_detail', kwargs={'building_slug': building_slug, 'apartment_pk': apartment_pk})

class VisitCreateView(CreateView):
	model = Visit
	template_name = "properties/create_visit.html"
	fields = ['visitor_name', 'visitor_lastname', 'visitor_identity']

	@method_decorator(permission_required("properties.add_visit", raise_exception=True))
	def dispatch(self, *args, **kwargs):
		return super(VisitCreateView, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(VisitCreateView, self).get_context_data(**kwargs)
		userprofile = get_user_profile(user=self.request.user)
		building_slug = self.kwargs['building_slug']
		building_object = Building.objects.get(slug__iexact=building_slug)
		context['building'] = building_object
		context['userprofile'] = userprofile
		return context

	def get_success_url(self, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		apto_pk = self.kwargs['apartment_pk']
		return reverse('property_detail', kwargs={
			'building_slug': building_slug,
			'apartment_pk': apto_pk
		})

	def form_valid(self, form, *args, **kwargs):
		building_slug = self.kwargs['building_slug']
		apto_pk = self.kwargs['apartment_pk']
		building_obj = Building.objects.get(slug__iexact=building_slug)
		apto_obj = Apartment.objects.get(pk=apto_pk, building=building_obj)
		form.instance.apartment = apto_obj
		form.instance.visit_date = timezone.now()
		return super(VisitCreateView, self).form_valid(form)


#FUNCTIONS TO IMPORT AND EXPORT PROPERTIES DATA WITH DJANGO-IMPORT-EXPORT

class ApartmentResource(resources.ModelResource):

    def before_import(self, dataset, dry_run=True, **kwargs):
        """
        Make standard corrections to the dataset before displaying to user
        """
        building=kwargs['building']
        dataset.append_col([None for row in range(len(dataset))], header='id')
        dataset.append_col([building.id for row in range(len(dataset))], header='building')

    class Meta:
        model = Apartment
        fields = ('id', 'name', 'area', 'registration_tag', 'building', 'parking_number', 'coefficient')


class ApartmentExport(View):

    def get(self, *args, **kwargs ):
        dataset = ApartmentResource().export()
        response = HttpResponse(dataset.csv, content_type="csv")
        response['Content-Disposition'] = 'attachment; filename=filename.csv'
        return response


class ApartmentImport(View):
    model = Apartment
    from_encoding = "utf-8"

    #: import / export formats
    DEFAULT_FORMATS = (
        base_formats.CSV,
        base_formats.XLS,
        base_formats.TSV,
        base_formats.ODS,
        base_formats.JSON,
        base_formats.YAML,
        base_formats.HTML,
    )
    formats = DEFAULT_FORMATS
    #: template for import view
    import_template_name = 'properties/import.html'
    resource_class = ApartmentResource

    def get_import_formats(self):
        """
        Returns available import formats.
        """
        return [f for f in self.formats if f().can_import()]

    def get_resource_class(self):
        if not self.resource_class:
            return modelresource_factory(self.model)
        else:
            return self.resource_class

    def get_import_resource_class(self):
        """
        Returns ResourceClass to use for import.
        """
        return self.get_resource_class()

    def get(self, *args, **kwargs ):
        '''
        Perform a dry_run of the import to make sure the import will not
        result in errors.  If there where no error, save the user
        uploaded file to a local temp file that will be used by
        'process_import' for the actual import.
        '''
        resource = self.get_import_resource_class()()
        building_slug = self.kwargs['building_slug']
        building_object = get_object_or_404(Building, slug__iexact=building_slug)

        context = {}
        context['building'] = building_object

        import_formats = self.get_import_formats()
        form = ImportForm(import_formats,
                          self.request.POST or None,
                          self.request.FILES or None)

        if self.request.POST and form.is_valid():
            input_format = import_formats[
                int(form.cleaned_data['input_format'])
            ]()
            import_file = form.cleaned_data['import_file']
            # first always write the uploaded file to disk as it may be a
            # memory file or else based on settings upload handlers
            with tempfile.NamedTemporaryFile(delete=False) as uploaded_file:
                for chunk in import_file.chunks():
                    uploaded_file.write(chunk)

            # then read the file, using the proper format-specific mode
            with open(uploaded_file.name,
                      input_format.get_read_mode()) as uploaded_import_file:
                # warning, big files may exceed memory
                data = uploaded_import_file.read()
                if not input_format.is_binary() and self.from_encoding:
                    data = force_text(data, self.from_encoding)
                dataset = input_format.create_dataset(data)
                result = resource.import_data(dataset, dry_run=True,
                                              raise_errors=False, building=building_object)

            context['result'] = result

            if not result.has_errors():
                context['confirm_form'] = ConfirmImportForm(initial={
                    'import_file_name': os.path.basename(uploaded_file.name),
                    'original_file_name': uploaded_file.name,
                    'input_format': form.cleaned_data['input_format'],
                })

        context['form'] = form
        context['opts'] = self.model._meta
        context['fields'] = [f.column_name for f in resource.get_fields()]

        return TemplateResponse(self.request, [self.import_template_name], context)


    def post(self, *args, **kwargs ):
        '''
        Perform a dry_run of the import to make sure the import will not
        result in errors.  If there where no error, save the user
        uploaded file to a local temp file that will be used by
        'process_import' for the actual import.
        '''
        resource = self.get_import_resource_class()()#
        building_slug = self.kwargs['building_slug']
        building_object = get_object_or_404(Building, slug__iexact=building_slug)

        context = {}
        context['building'] = building_object

        import_formats = self.get_import_formats()
        form = ImportForm(import_formats,
                          self.request.POST or None,
                          self.request.FILES or None)

        if self.request.POST and form.is_valid():
            input_format = import_formats[
                int(form.cleaned_data['input_format'])
            ]()
            import_file = form.cleaned_data['import_file']
            # first always write the uploaded file to disk as it may be a
            # memory file or else based on settings upload handlers
            with tempfile.NamedTemporaryFile(delete=False) as uploaded_file:
                for chunk in import_file.chunks():
                    uploaded_file.write(chunk)

            # then read the file, using the proper format-specific mode
            with open(uploaded_file.name,
                      input_format.get_read_mode()) as uploaded_import_file:
                # warning, big files may exceed memory
                data = uploaded_import_file.read()
                if not input_format.is_binary() and self.from_encoding:
                    data = force_text(data, self.from_encoding)
                dataset = input_format.create_dataset(data)
                result = resource.import_data(dataset, dry_run=True,
                                              raise_errors=False, building=building_object)

            context['result'] = result

            if not result.has_errors():
                context['confirm_form'] = ConfirmImportForm(initial={
                    'import_file_name': os.path.basename(uploaded_file.name),
                    'original_file_name': uploaded_file.name,
                    'input_format': form.cleaned_data['input_format'],
                })

        context['form'] = form
        context['opts'] = self.model._meta
        context['fields'] = [f.column_name for f in resource.get_fields()]

        return TemplateResponse(self.request, [self.import_template_name], context)

class ApartmentProcessImport(View):
    model = Apartment
    from_encoding = "utf-8"

    #: import / export formats
    DEFAULT_FORMATS = (
        base_formats.CSV,
        base_formats.XLS,
        base_formats.TSV,
        base_formats.ODS,
        base_formats.JSON,
        base_formats.YAML,
        base_formats.HTML,
    )
    formats = DEFAULT_FORMATS
    #: template for import view
    import_template_name = 'properties/import.html'
    resource_class = ApartmentResource

    def get_import_formats(self):
        """
        Returns available import formats.
        """
        return [f for f in self.formats if f().can_import()]

    def get_resource_class(self):
        if not self.resource_class:
            return modelresource_factory(self.model)
        else:
            return self.resource_class

    def get_import_resource_class(self):
        """
        Returns ResourceClass to use for import.
        """
        return self.get_resource_class()

    def post(self, *args, **kwargs):
        '''
        Perform the actual import action (after the user has confirmed he
    wishes to import)
        '''
        opts = self.model._meta
        resource = self.get_import_resource_class()()
        building_slug = self.kwargs['building_slug']
        building_object = get_object_or_404(Building, slug__iexact=building_slug)

        confirm_form = ConfirmImportForm(self.request.POST)
        if confirm_form.is_valid():
            import_formats = self.get_import_formats()
            input_format = import_formats[
                int(confirm_form.cleaned_data['input_format'])
            ]()
            import_file_name = os.path.join(
                tempfile.gettempdir(),
                confirm_form.cleaned_data['import_file_name']
            )
            import_file = open(import_file_name, input_format.get_read_mode())
            data = import_file.read()
            if not input_format.is_binary() and self.from_encoding:
                data = force_text(data, self.from_encoding)
            dataset = input_format.create_dataset(data)
            result = resource.import_data(dataset, dry_run=False,
                                 raise_errors=True, building=building_object)

            # Add imported objects to LogEntry
            #ADDITION = 1
            #CHANGE = 2
            #DELETION = 3
            #logentry_map = {
            #    RowResult.IMPORT_TYPE_NEW: ADDITION,
            #    RowResult.IMPORT_TYPE_UPDATE: CHANGE,
            #    RowResult.IMPORT_TYPE_DELETE: DELETION,
            #}
            #content_type_id=ContentType.objects.get_for_model(self.model).pk
            #'''
            #for row in result:
            #    LogEntry.objects.log_action(
            #        user_id=request.user.pk,
            #        content_type_id=content_type_id,
            #        object_id=row.object_id,
            #        object_repr=row.object_repr,
            #        action_flag=logentry_map[row.import_type],
            #        change_message="%s through import_export" % row.import_type,
            #    )
            #'''
            success_message = ('Import finished')
            messages.success(self.request, success_message)
            import_file.close()
            
            try:
                if mixpanel:
                    mixpanel.track(self.request.user.id, 'Registro de propiedades completado')
            except Exception as error:
                logger.error("No se pudo trackear el evento de registro de propiedades en Mixpanel: {0}".format(error), exc_info=True, extra={
                    'request': self.request,
                })

            return redirect('create_budget', building_slug=building_slug)
        else:
            print (confirm_form.errors)


#FUNCTIONS TO IMPORT-EXPORT OWNERS

class OwnerResource(resources.ModelResource):

    def before_import(self, dataset, dry_run=True, **kwargs):
        """
        Make standard corrections to the dataset before displaying to user
        """
        building=kwargs['building']
        lol = dataset.get_col(0)[0]
        print ("GONNA PRINT TYPE")
        print (type(lol))
        print ("columna 0: {0}".format(dataset.get_col(0)[0]))
        print ("columna 1: {0}".format(dataset.get_col(1)[0]))
        print ("columna 2: {0}".format(dataset.get_col(2)[0]))
        print ("columna 3: {0}".format(dataset.get_col(3)[0]))
        print ("columna 4: {0}".format(dataset.get_col(4)[0]))
        print ("columna 5: {0}".format(dataset.get_col(5)[0]))
        print ("columna 6: {0}".format(dataset.get_col(6)[0]))
        print ("columna 7: {0}".format(dataset.get_col(7)[0]))
        print ("BUILDING: ")
        print (building)
        i = 0
        last = dataset.height - 1

        # for all lines, search for id of family given and add a new line at the bottom with it then delete the first one
        while i <= last:
            id_owner = ''
            # Check if the Aparment exists in DB
            apartments_names = dataset.get_col(0)[0].split(",")
            apartments_existing_ids = []
            for name in apartments_names:
                try:
                    apto = Apartment.objects.get(name=name, building=building)
                    apartments_existing_ids.append(apto.id)
                except Apartment.DoesNotExist:
                    raise Exception("Apartment not in DB !")                   
                except Apartment.MultipleObjectsReturned:
                    pass

            apartments_existing_ids = ','.join(str(apto_id) for apto_id in apartments_existing_ids)

            def convert_data(string):
                string = str(string.lower())
                string = string.replace("í", "i")
                if string == "si":
                    return 1
                elif string == "no" or string == None:
                    return 0

            has_pet = dataset.get_col(7)[0]
            has_pet = convert_data(has_pet)
            is_inhabitant = dataset.get_col(8)[0]
            is_inhabitant = convert_data(is_inhabitant)

            dataset.rpush((apartments_existing_ids,
                dataset.get_col(1)[0],
                dataset.get_col(2)[0],
                dataset.get_col(3)[0],
                dataset.get_col(4)[0],
                dataset.get_col(5)[0],
                dataset.get_col(6)[0],
                has_pet,
                is_inhabitant))
            dataset.lpop()
            i = i + 1

        try:
            dataset.append_col([None for row in range(len(dataset))], header='id')
        except:
            print ("PUTTO ERROR")

    class Meta:
        model = Owner
        fields = ('id', 'apartments', 'address', 'email', 'name', 'last_name', 'phone_number', 'identification', 'has_pet', 'is_inhabitant')

#class OwnerResourceForExport(resources.ModelResource):
#    name = fields.Field(column_name='apartments', readonly=True)
#    address = fields.Field(column_name='address')
#    email = fields.Field(column_name='email')
#    last_name = fields.Field(column_name='last_name')
#    phone_number = fields.Field(column_name='phone_number')
#    identification = fields.Field(column_name='identification')
#    has_pet = fields.Field(column_name='has_pet')
#    is_inhabitant = fields.Field(column_name='is_inhabitant')


#    class Meta:
#        model = Apartment
#        fields = ("name")
#        export_order = ("name", "address", "email", "last_name", "phone_number", "identification", "has_pet", "is_inhabitant")


class OwnerExport(View):

    def get(self, *args, **kwargs ):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'

        writer = csv.writer(response)
        building_slug = self.kwargs["building_slug"]
        building_object = get_object_or_404(Building, slug__iexact=building_slug)
        apartments_list = Apartment.objects.filter(building=building_object)
        writer.writerow(['apartments', 'address', 'email', 'last_name', 'name', 'phone_number', 'identification', 'has_pet', 'is_inhabitant'])
        for apto in apartments_list:
            writer.writerow([apto.name])

        return response


class OwnerImport(View):
    model = Owner
    from_encoding = "utf-8"

    #: import / export formats
    DEFAULT_FORMATS = (
        base_formats.CSV,
        base_formats.XLS,
        base_formats.TSV,
        base_formats.ODS,
        base_formats.JSON,
        base_formats.YAML,
        base_formats.HTML,
    )
    formats = DEFAULT_FORMATS
    #: template for import view
    import_template_name = 'properties/import_owners.html'
    resource_class = OwnerResource

    def get_import_formats(self):
        """
        Returns available import formats.
        """
        return [f for f in self.formats if f().can_import()]

    def get_resource_class(self):
        if not self.resource_class:
            return modelresource_factory(self.model)
        else:
            return self.resource_class

    def get_import_resource_class(self):
        """
        Returns ResourceClass to use for import.
        """
        return self.get_resource_class()

    def get(self, *args, **kwargs ):
        '''
        Perform a dry_run of the import to make sure the import will not
        result in errors.  If there where no error, save the user
        uploaded file to a local temp file that will be used by
        'process_import' for the actual import.
        '''
        resource = self.get_import_resource_class()()
        building_slug = self.kwargs['building_slug']
        building_object = get_object_or_404(Building, slug__iexact=building_slug)

        context = {}
        context['building'] = building_object

        import_formats = self.get_import_formats()
        form = ImportForm(import_formats,
                          self.request.POST or None,
                          self.request.FILES or None)

        if self.request.POST and form.is_valid():
            input_format = import_formats[
                int(form.cleaned_data['input_format'])
            ]()
            import_file = form.cleaned_data['import_file']
            # first always write the uploaded file to disk as it may be a
            # memory file or else based on settings upload handlers
            with tempfile.NamedTemporaryFile(delete=False) as uploaded_file:
                for chunk in import_file.chunks():
                    uploaded_file.write(chunk)

            # then read the file, using the proper format-specific mode
            with open(uploaded_file.name,
                      input_format.get_read_mode()) as uploaded_import_file:
                # warning, big files may exceed memory
                data = uploaded_import_file.read()
                if not input_format.is_binary() and self.from_encoding:
                    data = force_text(data, self.from_encoding)
                dataset = input_format.create_dataset(data)
                result = resource.import_data(dataset, dry_run=True,
                                              raise_errors=False, building=building_object)

            context['result'] = result

            if not result.has_errors():
                context['confirm_form'] = ConfirmImportForm(initial={
                    'import_file_name': os.path.basename(uploaded_file.name),
                    'original_file_name': uploaded_file.name,
                    'input_format': form.cleaned_data['input_format'],
                })

        context['form'] = form
        context['opts'] = self.model._meta
        context['fields'] = [f.column_name for f in resource.get_fields()]

        return TemplateResponse(self.request, [self.import_template_name], context)


    def post(self, *args, **kwargs ):
        '''
        Perform a dry_run of the import to make sure the import will not
        result in errors.  If there where no error, save the user
        uploaded file to a local temp file that will be used by
        'process_import' for the actual import.
        '''
        resource = self.get_import_resource_class()()#
        building_slug = self.kwargs['building_slug']
        building_object = get_object_or_404(Building, slug__iexact=building_slug)

        context = {}
        context['building'] = building_object

        import_formats = self.get_import_formats()
        form = ImportForm(import_formats,
                          self.request.POST or None,
                          self.request.FILES or None)

        if self.request.POST and form.is_valid():
            input_format = import_formats[
                int(form.cleaned_data['input_format'])
            ]()
            import_file = form.cleaned_data['import_file']
            # first always write the uploaded file to disk as it may be a
            # memory file or else based on settings upload handlers
            with tempfile.NamedTemporaryFile(delete=False) as uploaded_file:
                for chunk in import_file.chunks():
                    uploaded_file.write(chunk)

            # then read the file, using the proper format-specific mode
            with open(uploaded_file.name,
                      input_format.get_read_mode()) as uploaded_import_file:
                # warning, big files may exceed memory
                data = uploaded_import_file.read()
                if not input_format.is_binary() and self.from_encoding:
                    data = force_text(data, self.from_encoding)
                dataset = input_format.create_dataset(data)
                result = resource.import_data(dataset, dry_run=True,
                                              raise_errors=False, building=building_object)

            context['result'] = result

            if not result.has_errors():
                context['confirm_form'] = ConfirmImportForm(initial={
                    'import_file_name': os.path.basename(uploaded_file.name),
                    'original_file_name': uploaded_file.name,
                    'input_format': form.cleaned_data['input_format'],
                })

        context['form'] = form
        context['opts'] = self.model._meta
        context['fields'] = [f.column_name for f in resource.get_fields()]

        return TemplateResponse(self.request, [self.import_template_name], context)

class OwnerProcessImport(View):
    model = Owner
    from_encoding = "utf-8"

    #: import / export formats
    DEFAULT_FORMATS = (
        base_formats.CSV,
        base_formats.XLS,
        base_formats.TSV,
        base_formats.ODS,
        base_formats.JSON,
        base_formats.YAML,
        base_formats.HTML,
    )
    formats = DEFAULT_FORMATS
    #: template for import view
    import_template_name = 'properties/import_owners.html'
    resource_class = OwnerResource

    def get_import_formats(self):
        """
        Returns available import formats.
        """
        return [f for f in self.formats if f().can_import()]

    def get_resource_class(self):
        if not self.resource_class:
            return modelresource_factory(self.model)
        else:
            return self.resource_class

    def get_import_resource_class(self):
        """
        Returns ResourceClass to use for import.
        """
        return self.get_resource_class()

    def post(self, *args, **kwargs):
        '''
        Perform the actual import action (after the user has confirmed he
    wishes to import)
        '''
        opts = self.model._meta
        resource = self.get_import_resource_class()()
        building_slug = self.kwargs['building_slug']
        building_object = get_object_or_404(Building, slug__iexact=building_slug)

        confirm_form = ConfirmImportForm(self.request.POST)
        if confirm_form.is_valid():
            import_formats = self.get_import_formats()
            input_format = import_formats[
                int(confirm_form.cleaned_data['input_format'])
            ]()
            import_file_name = os.path.join(
                tempfile.gettempdir(),
                confirm_form.cleaned_data['import_file_name']
            )
            import_file = open(import_file_name, input_format.get_read_mode())
            data = import_file.read()
            if not input_format.is_binary() and self.from_encoding:
                data = force_text(data, self.from_encoding)
            dataset = input_format.create_dataset(data)
            result = resource.import_data(dataset, dry_run=False,
                                 raise_errors=True, building=building_object)

            # Add imported objects to LogEntry
            #ADDITION = 1
            #CHANGE = 2
            #DELETION = 3
            #logentry_map = {
            #    RowResult.IMPORT_TYPE_NEW: ADDITION,
            #    RowResult.IMPORT_TYPE_UPDATE: CHANGE,
            #    RowResult.IMPORT_TYPE_DELETE: DELETION,
            #}
            #content_type_id=ContentType.objects.get_for_model(self.model).pk
            #'''
            #for row in result:
            #    LogEntry.objects.log_action(
            #        user_id=request.user.pk,
            #        content_type_id=content_type_id,
            #        object_id=row.object_id,
            #        object_repr=row.object_repr,
            #        action_flag=logentry_map[row.import_type],
            #        change_message="%s through import_export" % row.import_type,
            #    )
            #'''
            success_message = ('Import finished')
            messages.success(self.request, success_message)
            import_file.close()
            
            return redirect('properties', building_slug=building_slug)
        else:
            print (confirm_form.errors)


#FUCNTIONS TO IMPORT-EXPORT INHABITANTS

class InhabitantResource(resources.ModelResource):

    def before_import(self, dataset, dry_run=True, **kwargs):
        """
        Make standard corrections to the dataset before displaying to user
        """
        building=kwargs['building']
        i = 0
        last = dataset.height - 1

        # for all lines, search for id of family given and add a new line at the bottom with it then delete the first one
        while i <= last:
            id_owner = ''
            # Check if the Aparment exists in DB
            apartments_names = dataset.get_col(0)[0].split(",")
            apartments_existing_ids = []
            for name in apartments_names:
                try:
                    apto = Apartment.objects.get(name=name, building=building)
                    apartments_existing_ids.append(apto.id)
                except Apartment.DoesNotExist:
                    raise Exception("Apartment not in DB !")                   
                except Apartment.MultipleObjectsReturned:
                    pass

            apartments_existing_ids = ','.join(str(apto_id) for apto_id in apartments_existing_ids)

            dataset.rpush((apartments_existing_ids,
                dataset.get_col(1)[0],
                dataset.get_col(2)[0],
                dataset.get_col(3)[0],
                dataset.get_col(4)[0]))
            dataset.lpop()
            i = i + 1

        try:
            dataset.append_col([None for row in range(len(dataset))], header='id')
        except:
            print ("PUTTO ERROR")

    class Meta:
        model = Inhabitant
        fields = ('id', 'apartments', 'email', 'name', 'last_name', 'phone_number')


class InhabitantExport(View):

    def get(self, *args, **kwargs ):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'

        writer = csv.writer(response)
        building_slug = self.kwargs["building_slug"]
        building_object = get_object_or_404(Building, slug__iexact=building_slug)
        apartments_list = Apartment.objects.filter(building=building_object)
        writer.writerow(['apartments', 'email', 'last_name', 'name', 'phone_number'])
        for apto in apartments_list:
            writer.writerow([apto.name])

        return response


class InhabitantImport(View):
    model = Inhabitant
    from_encoding = "utf-8"

    #: import / export formats
    DEFAULT_FORMATS = (
        base_formats.CSV,
        base_formats.XLS,
        base_formats.TSV,
        base_formats.ODS,
        base_formats.JSON,
        base_formats.YAML,
        base_formats.HTML,
    )
    formats = DEFAULT_FORMATS
    #: template for import view
    import_template_name = 'properties/import_inhabitants.html'
    resource_class = InhabitantResource

    def get_import_formats(self):
        """
        Returns available import formats.
        """
        return [f for f in self.formats if f().can_import()]

    def get_resource_class(self):
        if not self.resource_class:
            return modelresource_factory(self.model)
        else:
            return self.resource_class

    def get_import_resource_class(self):
        """
        Returns ResourceClass to use for import.
        """
        return self.get_resource_class()

    def get(self, *args, **kwargs ):
        '''
        Perform a dry_run of the import to make sure the import will not
        result in errors.  If there where no error, save the user
        uploaded file to a local temp file that will be used by
        'process_import' for the actual import.
        '''
        resource = self.get_import_resource_class()()
        building_slug = self.kwargs['building_slug']
        building_object = get_object_or_404(Building, slug__iexact=building_slug)

        context = {}
        context['building'] = building_object

        import_formats = self.get_import_formats()
        form = ImportForm(import_formats,
                          self.request.POST or None,
                          self.request.FILES or None)

        if self.request.POST and form.is_valid():
            input_format = import_formats[
                int(form.cleaned_data['input_format'])
            ]()
            import_file = form.cleaned_data['import_file']
            # first always write the uploaded file to disk as it may be a
            # memory file or else based on settings upload handlers
            with tempfile.NamedTemporaryFile(delete=False) as uploaded_file:
                for chunk in import_file.chunks():
                    uploaded_file.write(chunk)

            # then read the file, using the proper format-specific mode
            with open(uploaded_file.name,
                      input_format.get_read_mode()) as uploaded_import_file:
                # warning, big files may exceed memory
                data = uploaded_import_file.read()
                if not input_format.is_binary() and self.from_encoding:
                    data = force_text(data, self.from_encoding)
                dataset = input_format.create_dataset(data)
                result = resource.import_data(dataset, dry_run=True,
                                              raise_errors=False, building=building_object)

            context['result'] = result

            if not result.has_errors():
                context['confirm_form'] = ConfirmImportForm(initial={
                    'import_file_name': os.path.basename(uploaded_file.name),
                    'original_file_name': uploaded_file.name,
                    'input_format': form.cleaned_data['input_format'],
                })

        context['form'] = form
        context['opts'] = self.model._meta
        context['fields'] = [f.column_name for f in resource.get_fields()]

        return TemplateResponse(self.request, [self.import_template_name], context)


    def post(self, *args, **kwargs ):
        '''
        Perform a dry_run of the import to make sure the import will not
        result in errors.  If there where no error, save the user
        uploaded file to a local temp file that will be used by
        'process_import' for the actual import.
        '''
        resource = self.get_import_resource_class()()#
        building_slug = self.kwargs['building_slug']
        building_object = get_object_or_404(Building, slug__iexact=building_slug)

        context = {}
        context['building'] = building_object

        import_formats = self.get_import_formats()
        form = ImportForm(import_formats,
                          self.request.POST or None,
                          self.request.FILES or None)

        if self.request.POST and form.is_valid():
            input_format = import_formats[
                int(form.cleaned_data['input_format'])
            ]()
            import_file = form.cleaned_data['import_file']
            # first always write the uploaded file to disk as it may be a
            # memory file or else based on settings upload handlers
            with tempfile.NamedTemporaryFile(delete=False) as uploaded_file:
                for chunk in import_file.chunks():
                    uploaded_file.write(chunk)

            # then read the file, using the proper format-specific mode
            with open(uploaded_file.name,
                      input_format.get_read_mode()) as uploaded_import_file:
                # warning, big files may exceed memory
                data = uploaded_import_file.read()
                if not input_format.is_binary() and self.from_encoding:
                    data = force_text(data, self.from_encoding)
                dataset = input_format.create_dataset(data)
                result = resource.import_data(dataset, dry_run=True,
                                              raise_errors=False, building=building_object)

            context['result'] = result

            if not result.has_errors():
                context['confirm_form'] = ConfirmImportForm(initial={
                    'import_file_name': os.path.basename(uploaded_file.name),
                    'original_file_name': uploaded_file.name,
                    'input_format': form.cleaned_data['input_format'],
                })

        context['form'] = form
        context['opts'] = self.model._meta
        context['fields'] = [f.column_name for f in resource.get_fields()]

        return TemplateResponse(self.request, [self.import_template_name], context)

class InhabitantProcessImport(View):
    model = Inhabitant
    from_encoding = "utf-8"

    #: import / export formats
    DEFAULT_FORMATS = (
        base_formats.CSV,
        base_formats.XLS,
        base_formats.TSV,
        base_formats.ODS,
        base_formats.JSON,
        base_formats.YAML,
        base_formats.HTML,
    )
    formats = DEFAULT_FORMATS
    #: template for import view
    import_template_name = 'properties/import_inhabitants.html'
    resource_class = InhabitantResource

    def get_import_formats(self):
        """
        Returns available import formats.
        """
        return [f for f in self.formats if f().can_import()]

    def get_resource_class(self):
        if not self.resource_class:
            return modelresource_factory(self.model)
        else:
            return self.resource_class

    def get_import_resource_class(self):
        """
        Returns ResourceClass to use for import.
        """
        return self.get_resource_class()

    def post(self, *args, **kwargs):
        '''
        Perform the actual import action (after the user has confirmed he
    wishes to import)
        '''
        opts = self.model._meta
        resource = self.get_import_resource_class()()
        building_slug = self.kwargs['building_slug']
        building_object = get_object_or_404(Building, slug__iexact=building_slug)

        confirm_form = ConfirmImportForm(self.request.POST)
        if confirm_form.is_valid():
            import_formats = self.get_import_formats()
            input_format = import_formats[
                int(confirm_form.cleaned_data['input_format'])
            ]()
            import_file_name = os.path.join(
                tempfile.gettempdir(),
                confirm_form.cleaned_data['import_file_name']
            )
            import_file = open(import_file_name, input_format.get_read_mode())
            data = import_file.read()
            if not input_format.is_binary() and self.from_encoding:
                data = force_text(data, self.from_encoding)
            dataset = input_format.create_dataset(data)
            result = resource.import_data(dataset, dry_run=False,
                                 raise_errors=True, building=building_object)

            # Add imported objects to LogEntry
            #ADDITION = 1
            #CHANGE = 2
            #DELETION = 3
            #logentry_map = {
            #    RowResult.IMPORT_TYPE_NEW: ADDITION,
            #    RowResult.IMPORT_TYPE_UPDATE: CHANGE,
            #    RowResult.IMPORT_TYPE_DELETE: DELETION,
            #}
            #content_type_id=ContentType.objects.get_for_model(self.model).pk
            #'''
            #for row in result:
            #    LogEntry.objects.log_action(
            #        user_id=request.user.pk,
            #        content_type_id=content_type_id,
            #        object_id=row.object_id,
            #        object_repr=row.object_repr,
            #        action_flag=logentry_map[row.import_type],
            #        change_message="%s through import_export" % row.import_type,
            #    )
            #'''
            success_message = ('Import finished')
            messages.success(self.request, success_message)
            import_file.close()
            
            return redirect('properties', building_slug=building_slug)