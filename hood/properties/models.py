from django.db import models
from django.template import defaultfilters
from import_export import resources
from buildings.models import Building

class Apartment(models.Model):
	building = models.ForeignKey(Building)
	area = models.IntegerField(help_text="Digite el area numericamente en m2", blank=True)
	registration_tag = models.CharField(max_length=100, blank=True)
	name = models.CharField(max_length=100)
	parking_number = models.CharField(max_length=100, blank=True)
	coefficient = models.DecimalField(max_digits=4, decimal_places=2)
	slug = models.SlugField(max_length=500)

	def save(self, *args, **kwargs):
		self.slug = defaultfilters.slugify(self.name)
		super(Apartment, self).save(*args, **kwargs)

	def get_owner(self):
		try:
			owner = self.owner_set.all()[0]
		except:
			owner = None
		return owner

	def get_latest_payment_registered(self):
		try:
			latest_payment = self.payment_set.filter(paid=True).order_by("date_of_payment").last()
			latest_payment_value = latest_payment.payment_record_date
		except Exception as lol:
			print (lol)
			latest_payment_value = None
		return latest_payment_value

	def __str__(self):
		return self.name

	def get_next_payment(self):
		try:
			next_payment = self.payment_set.filter(paid=False).order_by("date_of_payment").first()
		except Exception as lol:
			print (lol)
			next_payment = None
		return next_payment

class Owner(models.Model):
	apartments = models.ManyToManyField(Apartment)
	address = models.CharField(max_length=100)
	email = models.EmailField(max_length=254)
	last_name = models.CharField(max_length=100)
	name = models.CharField(max_length=100)
	phone_number = models.CharField(max_length=100)
	identification = models.CharField(max_length=100)
	has_pet = models.BooleanField(default=False)
	is_inhabitant = models.BooleanField(default=False)

	def __str__(self):
		return self.name + " " + self.last_name

class Inhabitant(models.Model):
	apartments = models.ManyToManyField(Apartment)
	email = models.EmailField(max_length=254)
	last_name = models.CharField(max_length=50)
	name = models.CharField(max_length=100)
	phone_number = models.CharField(max_length=100)
	
	def __str__(self):
		return self.name

class Visit(models.Model):
	visitor_name = models.CharField(max_length=100)
	visitor_lastname = models.CharField(max_length=100)
	visitor_identity = models.CharField(max_length=100)
	visit_timestamp = models.DateTimeField(auto_now_add=True)
	apartment = models.ForeignKey(Apartment)

	def __str__(self):
		return self.visitor_name

class AdministrativeCouncil(models.Model):
	building = models.ForeignKey(Building)
	members = models.ManyToManyField(Owner)

class ApartmentResource(resources.ModelResource):
    class Meta:
        model = Apartment