from django import forms
from .models import Apartment

class ApartmentUpdateForm(forms.ModelForm):
	class Meta:
		model = Apartment
		fields = ['name', 'registration_tag', 'area', 'parking_number']