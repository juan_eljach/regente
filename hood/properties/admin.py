from django.contrib import admin
from .models import Apartment, Inhabitant, Owner, Visit, AdministrativeCouncil
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from import_export import fields
from import_export import widgets

class ApartmentAdminResource(resources.ModelResource):

    def before_import(self, dataset, dry_run=True, **kwargs):
        """
        Make standard corrections to the dataset before displaying to user
        """
        dataset.append_col([None for row in range(len(dataset))], header='id')

    class Meta:
        model = Apartment
        fields = ('id', 'name', 'area', 'registration_tag', 'building', 'parking_number')

class ApartmentAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = ApartmentAdminResource
    exclude = ("slug",)
    list_display = ('building', '__str__')
    list_filter = ('building',)
    pass

class OwnerResource(resources.ModelResource):

    def before_import(self, dataset, dry_run=True, **kwargs):
        """
        Make standard corrections to the dataset before displaying to user
        """
        building_id = dataset.get_col(8)[0]
        del dataset["building"]

        
        i = 0
        last = dataset.height - 1

        # for all lines, search for id of family given and add a new line at the bottom with it then delete the first one
        while i <= last:
        #    # Check if the Owner exist in DB
            if (Owner.objects.filter(identification=dataset.get_col(7)[0])):
                id_owner = Owner.objects.filter(identification=dataset.get_col(7)[0])[0].id
            else:
                id_owner = ''
            # Check if the Aparment exists in DB
            apartments_names = dataset.get_col(1)[0].split(",")
            apartments_existing_ids = []
            for name in apartments_names:
                try:
                    apto = Apartment.objects.get(name=name, building=building_id)
                    apartments_existing_ids.append(apto.id)
                except Apartment.DoesNotExist:
                    raise Exception("Apartment not in DB !")                   
                except Apartment.MultipleObjectsReturned:
                    pass

            apartments_existing_ids = ','.join(str(apto_id) for apto_id in apartments_existing_ids)

            # use of "filter" instead of "get" to prevent duplicate values, select the first one in all cases
            dataset.rpush((id_owner, 
                apartments_existing_ids,
                dataset.get_col(2)[0],
                dataset.get_col(3)[0],
                dataset.get_col(4)[0],
                dataset.get_col(5)[0],
                dataset.get_col(6)[0],
                dataset.get_col(7)[0]))
            dataset.lpop()
            i = i + 1

    class Meta:
        model = Owner
        fields = ('id', 'apartments', 'address', 'email', 'name', 'last_name', 'phone_number', 'identification', 'has_pet')

class OwnerAdmin(ImportExportModelAdmin):
	resource_class = OwnerResource
	pass

class InhabitantResource(resources.ModelResource):

    def before_import(self, dataset, dry_run=True, **kwargs):
        """
        Make standard corrections to the dataset before displaying to user
        """
        building_id = dataset.get_col(6)[0]
        del dataset["building"]

        
        i = 0
        last = dataset.height - 1

        # for all lines, search for id of family given and add a new line at the bottom with it then delete the first one
        while i <= last:
            id_owner = ''
            # Check if the Aparment exists in DB
            apartments_names = dataset.get_col(1)[0].split(",")
            apartments_existing_ids = []
            for name in apartments_names:
                try:
                    apto = Apartment.objects.get(name=name, building=building_id)
                    apartments_existing_ids.append(apto.id)
                except Apartment.DoesNotExist:
                    raise Exception("Apartment not in DB !")                   
                except Apartment.MultipleObjectsReturned:
                    pass

            apartments_existing_ids = ','.join(str(apto_id) for apto_id in apartments_existing_ids)

            # use of "filter" instead of "get" to prevent duplicate values, select the first one in all cases
            dataset.rpush((id_owner, 
                apartments_existing_ids,
                dataset.get_col(2)[0],
                dataset.get_col(3)[0],
                dataset.get_col(4)[0],
                dataset.get_col(5)[0]))
            dataset.lpop()
            i = i + 1

    class Meta:
        model = Inhabitant
        fields = ('id', 'apartments', 'email', 'name', 'last_name', 'phone_number')

class InhabitantAdmin(ImportExportModelAdmin):
    resource_class = InhabitantResource
    pass

admin.site.register(Apartment, ApartmentAdmin)
admin.site.register(Inhabitant, InhabitantAdmin)
admin.site.register(Owner, OwnerAdmin)
admin.site.register(Visit)
admin.site.register(AdministrativeCouncil)
