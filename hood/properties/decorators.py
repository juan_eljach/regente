from .models import Building
from django.shortcuts import redirect
from functools import wraps
from django.http import HttpResponse
from django.core.urlresolvers import reverse

def verify_data(view_func):
    @wraps(view_func)
    def wrapper(request, *args, **kwargs):
        user = request.user
        building_slug = kwargs["building_slug"]
        building = Building.objects.get(slug__iexact=building_slug)
        if building.apartment_set.all().count() == 0:
            return redirect(reverse('properties_import', kwargs={'building_slug': building.slug}))
        return view_func(request, *args, **kwargs)
    return wrapper